<!-- TOC -->

- [说明](#说明)
  - [安装最新版脚本](#安装最新版脚本)
  - [关于鲲](#关于鲲)
  - [关于参数格式](#关于参数格式)
    - [类JSON格式](#类json格式)
    - [JSON参数的Base64编码](#json参数的base64编码)
  - [关于返回值](#关于返回值)
  - [关于eval的使用标准](#关于eval的使用标准)
  - [软件依赖](#软件依赖)
  - [关于 lib/function](#关于-libfunction)
    - [lib/function/all](#libfunctionall)
  - [待办事项](#待办事项)
  - [示例](#示例)
    - [安装包](#安装包)
    - [源码](#源码)
- [日志](#日志)
  - [2022](#2022)
    - [07.14.1507](#07141507)
    - [03.21.1451](#03211451)
  - [2021](#2021)
    - [11.11.1131](#11111131)
    - [10.11.1830](#10111830)
    - [09.02.1545](#09021545)
    - [08.19](#0819)
    - [08.06](#0806)
    - [08.01](#0801)
    - [07.30](#0730)

<!-- /TOC -->

# 说明

## 安装最新版脚本

执行下面命令将下载并安装最新安装包,并执行依赖安装:

```sh
curl -s -L https://gitlab.com/f97d1c/kun/-/raw/master/packages/install_latest.sh | bash
```

## 关于鲲

> 项目**鲲**致力于将常用内容进行脚本化操作.<br>
原则上适用于基于Linux以及Bash环境的所有发行版本.<br>
目前已在基于Debian的Ubuntu环境中得到基本验证.

## 关于参数格式

参数格式以JSON格式字符串为主,部分早期将逐步进行适配.

```sh
source "$kun_main_path/lib/other/print_log" '{"标题": "功能名,测试文件地址,测试对象地址,测试情景,测试结果,说明", "唯一标识": "lib_test_load_print_log", "高亮匹配": "异常"}'
```

JSON格式参数相较于Shell原有方式的优点在于:

0. 语义化 更简单明了知悉各参数功能
0. shell原有的长/短参数功能都较为复杂
0. JSON的适用性更广,后期如果需要多语言间相互配合,JSON格式无疑是最好的选择.

### 类JSON格式

由于shell自身的缺点以及JS的特性, 传参标准由标准JSON格式放宽到可适配JS对象字符串,如: 

```sh
source "$kun_main_path/lib/other/print_log" "{标题: '功能名,测试文件地址,测试对象地址,测试情景,测试结果,说明', 唯一标识: 'lib_test_load_print_log', 高亮匹配: '异常'}"
```

这种方式更便于shell动态生成参数字符串.

下面几种参数格式完全等价,同时也展示了类JSON格式的优势:

```sh
reg_str='异常'
'{"标题": "功能名,测试文件地址,测试对象地址,测试情景,测试结果,说明", "唯一标识": "lib_test_load_print_log", "高亮匹配": "异常"}'
"{\"标题\": \"功能名,测试文件地址,测试对象地址,测试情景,测试结果,说明\", \"唯一标识\": \"lib_test_load_print_log\", \"高亮匹配\": \"$reg_str\"}"
"{标题: '功能名,测试文件地址,测试对象地址,测试情景,测试结果,说明', 唯一标识: 'lib_test_load_print_log', 高亮匹配: '$reg_str'}"
```

### JSON参数的Base64编码

当函数的输出值为JSON格式并需要传递给其他函数处理时,<br>
直接传递JSON字符串,无论是空格还是双引号的处理都是相当麻烦的.

有一种方式是将JSON写入临时文件,其他函数读取文件后进行处理:

```sh
function log.print() {
  eval "export $(jq -r '.["变量"]' <<<"$(cat $1)")"
  # ....
}
```

当时要时刻注意临时文件的清理,频繁的将临时文件用作变量并不是好的习惯,至少不够优雅.

目前为止,最好的方案是进行Base64编码,编码后没有双引号,没有空格,没有临时文件,相对而言更为可取:

```sh
base64 -w 0 <<<'{ "名称": "测试用例", "描述": "针对described功能进行测试", "参数": { "仓库名称": { "类型": "string", "默认值": "", "变量名": "depository_name", "说明": "" }, "适配平台架构": { "类型": "string", "默认值": "linux/amd64,linux/arm64,linux/arm/v7", "变量名": "" }, "时区文件路径": { "类型": "string", "默认值": "/usr/share/zoneinfo/Asia/Shanghai", "变量名": "" }, "系统版本": { "类型": "decimal(2)", "默认值": "18.04", "说明": "", "变量名": "system_version" }, "镜像标签": { "类型": "string", "默认值": "ubuntu-18.04-20210919", "说明": "", "变量名": "image_tag" } } }'

# => eyAi5ZCN56ewIjogIua1i+ivleeUqOS+iyIsICLmj4/ov7AiOiAi6ZKI5a+5ZGVzY3JpYmVk5Yqf6IO96L+b6KGM5rWL6K+VIiwgIuWPguaVsCI6IHsgIuS7k+W6k+WQjeensCI6IHsgIuexu+WeiyI6ICJzdHJpbmciLCAi6buY6K6k5YC8IjogIiIsICLlj5jph4/lkI0iOiAiZGVwb3NpdG9yeV9uYW1lIiwgIuivtOaYjiI6ICIiIH0sICLpgILphY3lubPlj7DmnrbmnoQiOiB7ICLnsbvlnosiOiAic3RyaW5nIiwgIum7mOiupOWAvCI6ICJsaW51eC9hbWQ2NCxsaW51eC9hcm02NCxsaW51eC9hcm0vdjciLCAi5Y+Y6YeP5ZCNIjogIiIgfSwgIuaXtuWMuuaWh+S7tui3r+W+hCI6IHsgIuexu+WeiyI6ICJzdHJpbmciLCAi6buY6K6k5YC8IjogIi91c3Ivc2hhcmUvem9uZWluZm8vQXNpYS9TaGFuZ2hhaSIsICLlj5jph4/lkI0iOiAiIiB9LCAi57O757uf54mI5pysIjogeyAi57G75Z6LIjogImRlY2ltYWwoMikiLCAi6buY6K6k5YC8IjogIjE4LjA0IiwgIuivtOaYjiI6ICIiLCAi5Y+Y6YeP5ZCNIjogInN5c3RlbV92ZXJzaW9uIiB9LCAi6ZWc5YOP5qCH562+IjogeyAi57G75Z6LIjogInN0cmluZyIsICLpu5jorqTlgLwiOiAidWJ1bnR1LTE4LjA0LTIwMjEwOTE5IiwgIuivtOaYjiI6ICIiLCAi5Y+Y6YeP5ZCNIjogImltYWdlX3RhZyIgfSB9IH0K

base64 -d <<<'eyAi5ZCN56ewIjogIua1i+ivleeUqOS+iyIsICLmj4/ov7AiOiAi6ZKI5a+5ZGVzY3JpYmVk5Yqf6IO96L+b6KGM5rWL6K+VIiwgIuWPguaVsCI6IHsgIuS7k+W6k+WQjeensCI6IHsgIuexu+WeiyI6ICJzdHJpbmciLCAi6buY6K6k5YC8IjogIiIsICLlj5jph4/lkI0iOiAiZGVwb3NpdG9yeV9uYW1lIiwgIuivtOaYjiI6ICIiIH0sICLpgILphY3lubPlj7DmnrbmnoQiOiB7ICLnsbvlnosiOiAic3RyaW5nIiwgIum7mOiupOWAvCI6ICJsaW51eC9hbWQ2NCxsaW51eC9hcm02NCxsaW51eC9hcm0vdjciLCAi5Y+Y6YeP5ZCNIjogIiIgfSwgIuaXtuWMuuaWh+S7tui3r+W+hCI6IHsgIuexu+WeiyI6ICJzdHJpbmciLCAi6buY6K6k5YC8IjogIi91c3Ivc2hhcmUvem9uZWluZm8vQXNpYS9TaGFuZ2hhaSIsICLlj5jph4/lkI0iOiAiIiB9LCAi57O757uf54mI5pysIjogeyAi57G75Z6LIjogImRlY2ltYWwoMikiLCAi6buY6K6k5YC8IjogIjE4LjA0IiwgIuivtOaYjiI6ICIiLCAi5Y+Y6YeP5ZCNIjogInN5c3RlbV92ZXJzaW9uIiB9LCAi6ZWc5YOP5qCH562+IjogeyAi57G75Z6LIjogInN0cmluZyIsICLpu5jorqTlgLwiOiAidWJ1bnR1LTE4LjA0LTIwMjEwOTE5IiwgIuivtOaYjiI6ICIiLCAi5Y+Y6YeP5ZCNIjogImltYWdlX3RhZyIgfSB9IH0K'

# => { "名称": "测试用例", "描述": "针对described功能进行测试", "参数": { "仓库名称": { "类型": "string", "默认值": "", "变量名": "depository_name", "说明": "" }, "适配平台架构": { "类型": "string", "默认值": "linux/amd64,linux/arm64,linux/arm/v7", "变量名": "" }, "时区文件路径": { "类型": "string", "默认值": "/usr/share/zoneinfo/Asia/Shanghai", "变量名": "" }, "系统版本": { "类型": "decimal(2)", "默认值": "18.04", "说明": "", "变量名": "system_version" }, "镜像标签": { "类型": "string", "默认值": "ubuntu-18.04-20210919", "说明": "", "变量名": "image_tag" } } }
```


## 关于返回值

返回值|含义
-|-
return 0|成功
return 255|失败
return 127|表示部分成功(255/2),且对后续没有执行没有影响
echo json|当有需要返回值时在最后打印json字符串


不同于其他返回值统一格式([true, result]),shell函数只打印result,是否成功通过$?判断.

## 关于eval的使用标准

有些情况下eval是不得已的选择,为最大程度限制其能力,要添加命令前缀:

```bash
# eval "命令前缀 命令字符串"
eval "export $(jq -r '.["变量"]' <<<"$res")"
eval "echo $(jq -r '.["打印参数"]' <<<"$res")"
```

## 软件依赖

安装后如无法正常运行,尝试执行以下命令对相关依赖进行安装:

```sh
kun init
```

## 关于 lib/function

由于shell没有实例对象的概念,所以在调用new方法以后将对象内容以json格式写入临时文件,<br>
并将临时文件路径值返回调用方,并在每次调用时将实例ID作为参数1传入,函数加载对应临时文件并进行处理.

lib/function文件内所有方法名为文件名.方法含义格式,不允许存在全局变量,<br>
所有变量必须在函数内并定义为local类型.

### lib/function/all

lib/function/all用于加载function文件夹下所有.sh文件.<br>
遍历时首先判断kun_lib_function_文件名md5值 变量是否为空,空则进行加载.

## 待办事项

0. nodejs运行速度不太理想, 编写关于Json处理相关shell函数.
0. 早期功能添加测试用例
0. app/elasticsearch/可视化工具(ruby)进入终端后给出相关提示
0. 屏幕输出内容格式化处理
0. .deb打包项精细化
0. 早期功能逐渐转为JSON格式传参
0. described.show_how中两层for循环执行速度太慢.
0. 寻找关于单例方法重复调用更优方法
0. 关于防火墙开启状态下NFS端口相关配置脚本

## 示例

### 安装包

```sh
kun

+---------+---------+----------------+-----------+
| 参数1   | 参数2    | 参数3           | 快速拨号  |
+---------+---------+----------------+-----------+
| app     | tmux    | 初始化         | k000      |
| lib     | info    | 全部信息       | k001      |
| lib     | info    | 系统信息       | k002      |
| lib     | info    | 硬盘信息       | k003      |
| lib     | other   | 源码打包(deb)  | k004      |
| system  | 更新源   | 改为国内源      | k005      |
+---------+---------+----------------+-----------+

kun k005 # 等价于下面的完整参数
kun system 更新源 改为国内源
```

### 源码

```sh
# 源码根目录下
bash kun
bash kun 快速拨号值
bash kun system 更新源 改为国内源

# 源码根目录/system/更新源 目录下
bash 改为国内源.sh
```

# 日志

## 2022

### 07.14.1507

0. 交由[鲲贰零]项目发布版本.

### 03.21.1451

0. 新增 -功能-> app/python/\[Django]SQL转模型定义
0. 新增 -功能-> 网络/Dnsmasq/\[Ubuntu]\[服务端]安装
0. 新增 -功能-> 网络/Dnsmasq/\[Ubuntu]\[服务端]初始化
0. 调整 system/网络/释放端口 -重构-> 网络/其他/释放端口
0. 删除 -功能-> system/网络/释放端口
0. 新增 -功能-> 容器/docker-compose/Mysql
0. lib/other/backup_file -优化-> 被备份文件后缀判断及相关备份文件查找优化
0. 新增 -功能-> 应用/鲲/更新配置文件
0. 新增 -功能-> 网络/NFS/\[Ubuntu]\[服务端]安装
0. 新增 -功能-> 网络/NFS/\[服务端]启动
0. 新增 -功能-> 网络/NFS/\[服务端]检查状态
0. 新增 -功能-> 网络/NFS/\[服务端]更新共享目录
0. 新增 -功能-> 网络/NFS/查询共享目录
0. app/git/add_to_bashrc -优化-> git/https 将缓冲区增加到 500MB
0. 新增 -功能-> 网络/Samba/\[Ubuntu]\[服务端]安装
0. 新增 -功能-> 网络/Samba/\[Ubuntu]\[服务端]创建账户密码
0. 新增 -功能-> 网络/Samba/\[Ubuntu]\[服务端]更新共享目录
0. 新增 -功能-> 网络/Nethogs/\[Ubuntu]安装
0. 新增 -功能-> 网络/Nethogs/流量监控
0. 新增 -功能-> 网络/SFTP/\[服务端]\[Ubuntu]初始化
0. 新增 -功能-> 网络/SSH/创建标准配置文件

## 2021

### 11.11.1131

0. lib/other/backup_file -优化-> 备份文件名称格式调整及逻辑优化
0. 新增 -功能-> app/网易云/歌单内歌曲标识获取 
0. lib/function/similar_json -修复-> 字段值中存在:号导致格式化异常问题
0. lib/function/described -修复-> 默认去除字段值中空格问题
0. 新增 -功能-> app/docker/新建容器
0. 新增 -功能-> app/网易云/格式化标准文件名称
0. 新增 -功能-> lib/function/space_config
0. 新增 -功能-> lib/function/标准化文件名
0. 新增 -功能-> app/python/安装[Ubuntu]
0. app/ruby/安装 -重命名-> app/ruby/安装[Ubuntu]
0. app/dockerImage/ruby -优化-> 构建脚本及dockerfile微小调整
0. 新增 -功能-> app/dockerImage/python
0. lib/function/described.js -新增-> 参数说明 参数, 默认为false,当传值为true时可查看各参数相关说明.
### 10.11.1830

0. 新增 -功能-> lib/file/格式化Markdown
0. app/dockerImage/ubuntu -优化-> 增加对构建多架构镜像构建功能
0. app/ruby/安装 -优化-> 增加described描述
0. lib/function/described.js -优化-> 异常信息由字符串拼接改为数组追加
0. lib/function/described -调整-> 打印参数分隔符由:调整为@
0. 新增 -功能-> app/docker/初始化Buildx 构建多架构镜像必须容器
0. 新增 -功能-> app/dockerImage/ruby Ruby镜像构建功能
0. 新增 -功能-> lib/function/rescue 提供安全执行命令功能
0. 新增 -功能-> app/clash/安装 提供科学上网功能
0. 新增 -功能-> lib/function/all 加载全部通用方法
0. 新增 -功能-> lib/function/similar_json 类JSON格式对象处理功能(摆脱对NodeJs的依赖)
0. system/bashrc/标准化bashrc -优化-> 引入add_to_bashrc规则(在写入bashrc时 当存在名称为add_to_bashrc 名称文件时将该文件内容在最后追加)
0. 新增 -功能-> app/git/add_to_bashrc git相关需要写入bashrc内容
0. app/kun/依赖项安装 -调整-> 新增依赖 jq
0. lib/function/described -调整-> json字符串处理改为jq负责
0. lib/function/log -调整-> json字符串处理改为jq负责
0. lib/function/rescue -调整-> json字符串处理改为jq负责
0. 命令kun -调整-> 日志记录摆脱对 log 的依赖
0. 新增 -测试用例-> lib/test/lib_function_similar_json.test similar_json对应测试用例
0. lib/function/described -调整-> described.initialize返回结果由配置文件路径改为base64编码json字符串
0. lib/function/log -调整-> log.initialize返回结果由配置文件路径改为base64编码json字符串
0. lib/function/rescue -调整-> rescue.initialize返回结果由配置文件路径改为base64编码json字符串
0. lib/function/similar_json -新增功能-> similar_json.get 获取类JSON字符串键值
0. app/dockerImage/ubuntu -调整-> 构建模式调整为: 开发/准生产/生产
0. 新增 -功能-> system/网络/释放端口
0. app/dockerImage/ruby -调整-> 构建模式调整为: 开发/准生产/生产
0. 新增 -功能-> app/clash/终端代理变量
0. 新增 -功能-> app/git/代码量分析
0. app/git/add_to_bashrc -调整-> 增加缓存git账号命令

### 09.02.1545

> 本版针对构建镜像(app/dockerImage/ubuntu)进行了大量调整/优化/重构.

0. 包名格式 -调整-> 年.月.日.时分(kun_2021.09.02.1545.deb), 应对镜像构建过程中使用同名当日其他较早缓存.
0. 新增 -功能-> system/bashrc/目录创建别名
0. system/初始化/标准化bashrc -调整路径-> system/bashrc/标准化bashrc
0. 命令kun -修复-> 传参到节点脚本只有一个参数
0. 命令kun -调整-> 内容打印输出改为print_log
0. lib/function/params.js -修复-> 参数值为false返回必填项为空
0. lib/other/print_log#print_log -优化-> 传递参数1将对日志内容过滤,仅显示匹配参数1的内容
0. system/初始化/改为国内源 -优化-> 支持静默执行
0. 新增 -功能-> app/dockerImage/ubuntu
0. lib/function/params.js -重构-> lib/function/described 用来对脚本进行描述
0. system/初始化/bashrc -结构调整-> system/bashrc/bashrc
0. 新增 -功能-> system/debian/dragonService 一条龙式初始化系统环境
0. 新增 -功能-> system/初始化/新建用户
0. lib/other/print_log -修复-> 非默认间隔符状态下的显示问题
0. system/bashrc/标准化bashrc -优化-> 增加参数1指定被标准化用户
0. system/初始化/必要软件安装 -重构-> system/debian/dragonService
0. system/初始化/本地时间 -优化-> 对依赖软件进行检查
0. system/初始化/改为国内源 -优化-> 支持参数静默执行及必要参数检查
0. app/kun/源码打包\[Deb\] -优化-> 版本号精确到分钟
0. lib/other/print_log -重构-> lib/function/log 基于described的日志记录打印
0. 新增 -功能-> system/debian/安装系统必须项
0. 新增 -功能-> lib/test/lib_function_described.test described 回归测试文件
0. 新增 -功能-> lib/test/lib_function_log.test log回归测试文件
0. 新增 -功能-> app/kun/依赖项安装
0. 新增 -功能-> app/docker/删除无效镜像
0. system/初始化/新建用户 -优化-> 添加用户ID/用户名/用户组名/用户组ID是否已存在校验
0. system/debian/dragonService.sh -优化-> 添加described描述/执行情况记录优化
0. system/bashrc/标准化bashrc -优化-> 参数标准改为标准JSON格式
0. lib/test/all -调整-> 日志记录改为log
0. lib/function/log -优化-> 由于trap对于source文件的影响,清除临时文件改为手动模式(log.close).
0. lib/function/described -优化-> 由于trap对于source文件的影响,清除临时文件改为手动模式(described.close),同时添加 described.test 函数, described.validate变为调用described.test后执行described.close
0. lib/function/described.js -优化-> 添加变量/ARG变量/JSON参数三个通用属性,给定参数处理添加异常处理
0. app/tmux/初始化 -优化-> 参数标准改为标准JSON格式
0. app/dockerImage/ubuntu -优化-> 添加described描述/容器构建命令改为动态生成
0. app/dockerImage/ubuntu.dockerfile -优化-> 基础镜像版本/设定进入容器后所使用用户/进入容器后的默认目录根据ARG参数动态设定
0. 命令kun -调整-> 日志记录改为log

### 08.19

0. 快速拨号 -修复-> 无功能参数(params_func)情况下将$@带入子脚本中.
0. 新增 -功能-> app/elasticsearch/requestES
0. 新增 -功能-> app/elasticsearch/创建文档
0. 新增 -功能-> 引入回归测试流程
0. 新增 -功能-> lib/other/print_log

### 08.06

0. 新增 -功能-> app/ruby/安装
0. 新增 -功能-> app/elasticsearch/可视化工具(ruby)
0. lib/other/源码打包(deb) -调整-> app/kun/源码打包(deb)
0. 未给出完整三级参数时,根据给出参数进行过滤

### 08.01

0. system/更新源 -目录-> system/初始化
0. system/更新源/改为国内源 -功能-> system/初始化/改为国内源
0. 新增 -功能-> system/初始化/标准化bashrc
0. 新增 -功能-> system/初始化/标准化sysctl
0. 新增 -功能-> system/debian/清理缓存.sh
0. 新增 -功能-> system/debian/更新系统.sh
0. 新增 -功能-> system/初始化/本地时间
0. 新增 -功能-> app/docker/卸载
0. 新增 -功能-> app/docker/安装
0. 新增 -功能-> app/docker/普通用户管理
0. 新增 -功能-> app/docker/标准化daemon配置文件
0. 新增 -功能-> app/docker/设置开机启动

### 07.30

0. 为打印参数添加快速拨号,可通过代号替代详细信息参数内容
0. 打包功能集成至项目中