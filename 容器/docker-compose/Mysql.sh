#!/bin/bash --login

if [ ! $kun_main_path ]; then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

source "$kun_main_path/lib/function/all"

params="$@"
params=${params:-'{}'}

json '.镜像仓库' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  image_store=$(json '.镜像仓库' <<<"$params")
fi
image_store=${image_store:-'mysql/mysql-server'}

json '.mysql版本' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  mysql_version=$(json '.mysql版本' <<<"$params")
fi
mysql_version=${mysql_version:-'8.0.28'}

json '.容器名称' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  container_name=$(json '.容器名称' <<<"$params")
fi
container_name=${container_name:-"${image_store}-${mysql_version}"}
container_name=${container_name##*/}

json '.暴露端口' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  export_port=$(json '.暴露端口' <<<"$params")
fi
export_port=${export_port:-"3306"}

json '.存储路径' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  store_path=$(json '.存储路径' <<<"$params")
fi
store_path=${store_path:-"/home/$(id -u -n)/db/$container_name"}

json '.数据库密码' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  mysql_password=$(json '.数据库密码' <<<"$params")
fi
mysql_password=${mysql_password:-"$container_name"}

json '.数据库配置文件路径' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  mysql_cnf_path=$(json '.数据库配置文件路径' <<<"$params")
fi
mysql_cnf_path=${mysql_cnf_path:-"$kun_main_path/容器/docker-compose/my.cnf"}

cd "$kun_main_path"

desc_info=$(described.initialize "{
  名称: '创建容器',
  描述: '根据提供信息创建mysql镜像实例',
  参数: {
    镜像仓库: {类型: 'string', 默认值: '$image_store', 变量名: 'image_store', 说明: ''},
    mysql版本: {类型: 'version_number(3)', 默认值: '$mysql_version', 说明: '', 变量名: 'mysql_version'},
    容器名称: {类型: 'string', 默认值: '$container_name', 变量名: 'container_name', 说明: ''},
    暴露端口: {类型: 'integer', 默认值: '$export_port', 变量名: 'export_port', 说明: ''},
    存储路径: {类型: 'string', 默认值: '$store_path', 变量名: 'store_path', 说明: '数据库数据在宿主机的存储路径(绝对)'},
    数据库密码: {类型: 'string', 默认值: '$mysql_password', 变量名: 'mysql_password', 说明: ''},
    数据库配置文件路径: {类型: 'string', 默认值: '$mysql_cnf_path', 变量名: 'mysql_cnf_path', 说明: ''},
  }
}")

if [[ ! $? -eq 0 ]]; then
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

res=$(described.test $desc_info "$@")
if [[ ! $? -eq 0 ]]; then
  echo "$res"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

eval "export $(jq -r '."变量"' <<<"$res")"
eval "echo $(jq -r '."打印参数"' <<<"$res")"
echo -e "\n"

mkdir -p $store_path

docker_compose_file_path="./docker_compose_cnf_$(date +%Y-%m-%d-%H%M-%s).yml"

# 布尔变量包括 MYSQL_RANDOM_ROOT_PASSWORD、 MYSQL_ONETIME_PASSWORD、 MYSQL_ALLOW_EMPTY_PASSWORD和 ， MYSQL_LOG_CONSOLE 通过将它们设置为任何非零长度的字符串来使它们为真.因此，将它们设置为例如“ 0 ”、 “ false ”或“ no ”不会使它们为假，而是实际上使它们为真.这是一个已知的问题.

tee "$docker_compose_file_path" <<EOF
version: '3.3'
services:
  $container_name:
    image: ${image_store}:${mysql_version}
    container_name: $container_name
    environment:
      MYSQL_ROOT_PASSWORD: '$mysql_password'
      MYSQL_ONETIME_PASSWORD:
    ports:
      - '$export_port:3306'
    volumes:
      - $store_path:/var/lib/mysql
      - $mysql_cnf_path:/etc/mysql/mysql.conf.d/mysqld.cnf
EOF

docker-compose -f $docker_compose_file_path up -d

docker logs $container_name
docker logs $container_name 2>&1 | grep GENERATED

rm -f $docker_compose_file_path

# 容器连接
# docker exec -it $(docker ps | grep mysql | awk '{printf $NF}' | sort -r | head -n 1) mysql -uroot -p
# 本地连接(需开启远程访问权限)
# mysql -h 127.0.0.1 -uroot -p 

# 开启远程访问权限
# USE mysql;
# -- 修改权限
# update user set host = '%' where user = 'root';
# -- 清理缓存
# FLUSH PRIVILEGES;
# SELECT host,user,authentication_string FROM user;