/*
node params.js 给定参数 默认参数
默认参数中存在为空的属性表名该值为必填项,不能为空
*/


function JsonStr(str) {
  let jsonStr
  try {
    jsonStr = JSON.parse(str);
  } catch (e) {
    return [false, '给定字符串非JSON格式'];
  }
  return [true, jsonStr]
}

function returnResult(object) {
  return console.log(JSON.stringify(object));
}

let argvs = process.argv.slice(2);
if (!!!argvs[0]) {
  return returnResult([false, '参数1: 不能为空'])
}
let res = JsonStr(argvs[0])
if (!res[0]) return returnResult([false, "参数1: " + res[1]]);
let give_params = res[1]

res = JsonStr(argvs[1] || '{}')
if (!res[0]) return returnResult([false, "参数2: " + res[1]]);
let default_params = res[1]
let params = Object.assign(default_params, give_params)

for (let [key, value] of Object.entries(params)) {
  if (value === '') return returnResult([false, "参数1: " + key + ' 为必填项不能为空,'+value])
}

console.log(JSON.stringify([true, params]))
// console.table(params)

// const structDatas = [
//   { handler: 'http', endpoint: 'http://localhost:3000/path', method: 'ALL' },
//   { handler: 'event', endpoint: 'http://localhost:3000/event', method: 'POST' },
//   { handler: 'GCS', endpoint: 'http://localhost:3000/GCS', method: 'POST' }
// ];
// console.table(structDatas);