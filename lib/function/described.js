let argvs = process.argv.slice(2);

function returnResult(object) {
  return console.log(JSON.stringify(object));
}

function _typeof(value) {
  if (!!!value) return typeof value;
  // if (typeof value == 'string') return 'string' // 字符串类型

  let valueStr = value.toString()
  if (typeof value == 'object') {
    if (Array.isArray(value)) { // 数组对象类型
      return 'array'
    } else if (!!(value.__proto__.toString().match(/^\[object .*Element\]$/))) { // html元素类型
      return 'element'
    } else {
      return 'keyValue' // 键值对类型
    }
  } else if (valueStr.match(/^http(s|)\:\/\/.*/)) { // 链接类型
    return 'url'
  } else if (!!valueStr.match(/^(\-|)\d{1,}$/)) { // 数值类型(整数)
    return 'integer'
  } else if (!!valueStr.match(/^(\-|)\d{1,}\.{1}\d{0,2}$/)) { // 数值类型(2位小数)
    return 'decimal(2)'
  } else if (!!valueStr.match(/^(\-|)\d{1,}\.{1}\d{2,}$/)) { // 数值类型(大于2位小数)
    return 'decimal(2+)'
  } else if (!!valueStr.match(/^\d{4}\-\d{2}\-\d{2}$/)) { // 日期类型
    return 'date'
  } else if (!!valueStr.match(/^\d{4}\-\d{2}\-\d{2}T/)) { // 日期时间类型
    return 'dateTime'
  } else if (!!valueStr.match(/^\d{1,}\.\d{1,}\.\d{1,}$/)) { // 数字版本号(3段) 2.5.3
    return 'version_number(3)'
  } else { // 其他按原始类型
    return typeof value
  }
}

function initialize() {
  eval('var reality=' + argvs[1])

  let ideal = {
    名称: '',
    描述: '',
    参数: {},
    参数说明: false,
  }

  let self = {}
  // self = Object.assign({}, ideal)
  self = Object.assign(ideal, (reality || {}))

  if (!!!self.名称) return returnResult([false, "描述信息中[名称]参数不能为空"])
  if (!!!self.描述) return returnResult([false, "描述信息中[描述]参数不能为空"])


  returnResult([true, self])
}

function validate() {
  if (!!!argvs[1]) return returnResult([false, '参数1: 不能为空'])

  try {
    eval('var reality=' + argvs[1])
  } catch (error) {
    return returnResult([false, '参数1: 非JSON或类JSON格式 -> ' + argvs[1]])
  }

  let ideal = JSON.parse(argvs[2])
  let self = {}

  // 根据描述里面的默认值打底
  for (let [key, value] of Object.entries(ideal.参数)) {
    self[key] = value.默认值
  }

  // 根据实际传参进行覆盖
  for (let [key, value] of Object.entries(reality)) {
    self[key] = value
  }

  // 存在undefind的即为校验失败
  let error = []
  for (let [key, value] of Object.entries(self)) {
    if (self[key] === '' || self[key] == undefined) {
      error.push(key + '不能为空')
    }
  }

  if (error.length > 0) return returnResult([false, error.join(), self])

  // 参数校验
  for (let [key, value] of Object.entries(self)) {
    // 描述中未出现参数不作处理
    if (!!!ideal.参数[key]) continue;

    // 校验参数类型是否与描述相符
    let realType = _typeof(value)
    let idealType = (ideal.参数[key]['类型'] || '未描述参数类型')
    if (realType != idealType) {
      error.push(key + '参数类型不符, 要求: ' + idealType + ', 实际: ' + realType)
    }

  }
  if (error.length > 0) return returnResult([false, error.join(), self])

  // 根据描述中出现key生成self自身JSON字符串
  self['JSON参数'] = {}
  for (let [key, value] of Object.entries(ideal.参数)) {
    self['JSON参数'][key] = self[key]
  }
  self['JSON参数'] = JSON.stringify(self['JSON参数']).replace(/\s|\n/g, '')

  // 提供shell变量格式字符串
  self.变量 = ''
  self['ARG变量'] = ''
  for (let [key, value] of Object.entries(self)) {
    let described = (ideal.参数[key] || {})
    if (!!!described['变量名']) continue;
    self.变量 += described['变量名'] + "='" + value + "'; "
    self[described['变量名']] = value
    self['ARG变量'] += described['变量名'].toUpperCase() + '=' + value + ' '
  }

  if (self.参数说明) return returnResult([false, '可根据上述说明提供json格式参数值.', self])
  returnResult([true, self])
}

eval(argvs[0] + "()");
