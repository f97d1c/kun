#!/bin/bash --login

if [ ! $kun_main_path ]; then
  current_path=$(
    cd $(dirname $0)
    pwd
  )
  kun_main_path="$current_path/../../"
fi

source "$kun_main_path/lib/function/all"

log_config=$(log.initialize "{标题: '功能名,测试情景,测试结果,说明', 唯一标识: 'lib_test_load_log', 高亮匹配: '异常', 分隔符: ','}")

cd "$kun_main_path/lib/test"
find *.test -type f -print0 | while IFS= read -r -d $'\0' file; do
  source "$file"
  if [[ ! $? -eq 0 ]]; then
    log.set $log_config "测试文件加载, lib/test/$file,-,测试文件执行失败情况,异常,该脚本内部发生异常"
  fi
done

log.print $log_config

# bash lib/test/all.sh