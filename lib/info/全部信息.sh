#!/bin/bash --login

if [ ! $lib_path ]
then
  current_path=$(cd `dirname $0`;pwd)
  lib_path="$current_path/../"
fi

# TODO:优化: 重复加载同一文件问题以后处理
source "$lib_path/other/print_table"

print_content="参数,参数值\n"

# echo $(realpath "$0")

source "$lib_path/info/系统信息.sh"
source "$lib_path/info/硬盘信息.sh"

printf "\\e[2J\\e[H\\e[m"
print_table ',' $print_content