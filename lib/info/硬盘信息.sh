#!/bin/bash --login

if [ ! $lib_path ]
then
  current_path=$(cd `dirname $0`;pwd)
  lib_path="$current_path/../"
fi

source "$lib_path/other/print_table"

# 需root用户执行校验
if [ ! $USER == 'root' ]; then echo '失败：权限被拒绝'; exit 255; fi

# 是否安装smartmontools校验
if ! [ -x "$(command -v smartctl)" ]; then
  read -p "尚未安装smartmontools软件,是否进行安装?(y/n)" choice
  case $choice in
    y|Y|yes|YES)
      # if [ ! $system_name ]; then source "../other/system_info.sh" fi
      echo 'TODO: 适配各系统安装软件'
      exit 255
    ;;
    *)
      exit 255
    ;;
  esac
fi

if [ ! $print_content ]; then
  print_content="参数,参数值\n"
  print_detail=0
else  
  print_detail=1
fi

default_mount_point='/'
disk_mount_point=${disk_mount_point:-$default_mount_point}
print_content+="硬盘挂载点,$disk_mount_point\n"

disk_path=$(df -h | awk '$NF==mount_point{printf $1}' mount_point=$disk_mount_point)
print_content+="硬盘名称,$disk_path\n"

disk_info=$(sudo smartctl -A $disk_path)

function get_disk_info () {
  echo "$disk_info" | awk -F ':' 'NR>5{if ($1==item_name) print $2;}' item_name="$1" | awk '{gsub(/\s+|,/, ""); print $1}'
}

disk_temperature=$(get_disk_info 'Temperature')
print_content+="硬盘温度(摄氏度),$(echo $disk_temperature | awk '{gsub(/[^0-9]|,/, ""); print $1}')\n"

disk_data_units_read=$(get_disk_info 'Data Units Read')
print_content+="硬盘读取量,$disk_data_units_read\n"

disk_data_units_written=$(get_disk_info 'Data Units Written')
print_content+="硬盘写入量,$disk_data_units_written\n"

disk_power_cycles=$(get_disk_info 'Power Cycles')
print_content+="硬盘电源循环次数,$disk_power_cycles\n"

disk_power_on_hours=$(get_disk_info 'Power On Hours')
print_content+="硬盘通电时长(小时),$disk_power_on_hours\n"

disk_unsafe_shutdowns=$(get_disk_info 'Unsafe Shutdowns')
print_content+="硬盘不安全停机次数,$disk_unsafe_shutdowns\n"

if [ $print_detail -eq 0 ]; then
  printf "\\e[2J\\e[H\\e[m"
  print_table ',' $print_content
fi