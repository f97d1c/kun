#!/bin/bash --login

if [ ! $lib_path ]
then
  current_path=$(cd `dirname $0`;pwd)
  lib_path="$current_path/../"
fi

source "$lib_path/other/print_table"

if [ ! $print_content ]; then
  print_content="参数,参数值\n"
  print_detail=0
else  
  print_detail=1
fi

hardware_platform=$(uname -i)
print_content+="硬件架构,$hardware_platform\n"

cpu_vendor=$(cat /proc/cpuinfo | grep 'vendor' | uniq | awk '{print $3}')
print_content+="处理器制造商,$cpu_vendor\n"

cpu_model=$(cat /proc/cpuinfo | grep "model name\|Model" | uniq | awk '{gsub(/^(\w|\s){1,}([^:]):\s/,"");print}' | awk '{gsub(/\s/, "");print}')
print_content+="处理器型号,$cpu_model\n"

cpu_count=$(cat /proc/cpuinfo | grep processor | wc -l)
print_content+="处理器单元数量,$cpu_count\n"

system_name=$(cat /etc/issue | awk '{print tolower($1)}')
print_content+="系统名称,$system_name\n"

version_name=$(lsb_release -c -s)
print_content+="系统版本名称,$version_name\n"

system_version=$(cat /etc/issue | awk '{print $2}' | cut -d \. -f 1,2)
print_content+="系统版本号,$system_version\n"

kernel_release=$(uname -r)
print_content+="内核发行号,$kernel_release\n"

public_ip=$(curl -s -4 --connect-timeout 3 icanhazip.com)
print_content+="公共IP地址(IPV4),$public_ip\n"

private_ip=$(hostname -I | awk '{print $1}')
print_content+="私有IP地址,$private_ip\n"

memory_usage=$(free -m | awk 'NR==2{printf "%s/%sMB(%.2f%%)\n", $3,$2,$3*100/$2 }')
print_content+="内存情况,$memory_usage\n"

disk_usage=$(df -h | awk '$NF=="/"{printf "%d/%dGB(%s)\n", $3,$2,$5}')
print_content+="硬盘情况,$disk_usage\n"

cpu_load=$(top -bn1 | grep load | awk '{printf "%.2f\n", $(NF-2)}' )
print_content+="cpu负载,$cpu_load\n"

print_content+="系统当前时间,$(date +%Y年%m月%d日%H时%M分%S秒)\n"

if [ $print_detail -eq 0 ]; then
  printf "\\e[2J\\e[H\\e[m"
  print_table ',' $print_content
fi