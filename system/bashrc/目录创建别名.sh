#!/bin/bash --login

if [ ! "$1" ];then
  echo "请提供目录绝对路径"
  exit 255
fi

if [ ! -d "$1" ]; then 
  echo "提供路径不存在或非目录类型."
  exit 255
fi

current_path=$1
if [ ! "$2" ];then
  dir_name=${current_path##*/}
else 
  dir_name="$2"
fi

if [ -x "$(command -v $dir_name)" ]; then
  echo "存在同名命令无法添加, 或提供参数2指定其他别名"
  exit 255
fi

relevant_alias=$(cat /home/$USER/.bashrc | grep "alias $dir_name")

# 判断bash.rc里面是否已存在别名
if [ -n "$relevant_alias" ]; then
  used_alias=$(alias | grep $dir_name)
  # 判断正在使用的别名中是否存在
  if [ -n "$used_alias"]; then
    echo "bashrc中已存在同名别名,无需再次创建."
    exit 255
  else
    echo "bashrc中已存在同名别名,但未生效."
    exit 255
  fi
else
  echo "alias $dir_name=\"clear;cd ${current_path}\"" | sudo tee -a /home/$USER/.bashrc;
fi 