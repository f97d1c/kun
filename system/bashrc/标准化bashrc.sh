#!/bin/bash --login

if [ ! $kun_main_path ]; then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

params="$@"
params=${params:-"{}"}
user_name=$(json ".用户名" <<<"$params")
user_name=${user_name:-"$(id -u -n)"}

file_path="/home/$user_name/.bashrc"
source "$kun_main_path/lib/other/backup_file" $file_path
if [[ ! $? -eq 0 ]];then exit 255;fi

echo "替换为标准bashrc"
cp "$kun_main_path/system/bashrc/bashrc" $file_path

cd $kun_main_path
find */*/add_to_bashrc -type f -print0 | while IFS= read -r -d $'\0' file; do 
  dir_name=${file%/*}
  dir_1_name=${dir_name%/*}
  dir_2_name=${dir_name##*/}
  echo "$dir_2_name 相关内容追加"
  echo -e "\n\n# === $dir_2_name相关追加内容开始 ===" >> $file_path
  echo -e "\n$(cat $file)" >> $file_path
  echo -e "\n# === $dir_2_name相关追加内容结束 ===" >> $file_path
done

echo "完成替换, 需手动 source ~/.bashrc 或重开终端以生效."

# bash system/bashrc/标准化bashrc.sh