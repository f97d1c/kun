#!/bin/bash --login

if [ "$(id -u)" -ne 0 ]
  then echo "权限不足: 仅限root用户执行"
  exit 255
fi

file_id='dragonService'
file_name_prefix="kun_log_$file_id"
tmpfile_=$(mktemp -t "$file_name_prefix.XXXXXXX") || exit 255
trap 'rm -f "$tmpfile_"' EXIT
echo '序号@命令@描述@结果@说明' >>$tmpfile_

if [ ! $kun_main_path ]; then
  current_path=$(
    cd $(dirname $0)
    pwd
  )
  kun_main_path="$current_path/../.."
fi
# source "$kun_main_path/lib/function/all"

loop_count=0

function exec_step() {
  cmd=$1
  state='失败'
  mark=$3
  # eval $cmd
  echo "开始执行: $cmd"
  local result
  result=$(eval "$cmd")
  if [[ $? -eq 0 ]];then 
    state="成功";
    echo "$(printf '%03d' $loop_count)@${cmd:0:88}@$2@$state@$3" >>$tmpfile_
  else
    result_last_line=$(echo -e "$result" | tail -1)
    mark=${result_last_line:-$mark}
    printf "\033[0;31m\33[7m$(printf '%03d' $loop_count)@${cmd:0:88}@$2@$state@$mark\33[0m\033[0m\n" >>$tmpfile_
  fi
  echo -e "$result"
  echo "执行$state: $cmd"
  loop_count=$(expr $loop_count + 1)
}

function exec_install(){
  params=($@)
  exec_step "sudo apt-get install -fy $1" "安装$1" "${params[@]:1:${#params[@]}}"
}

function exec_source(){
  params=($@)
  params_func="${params[@]:1:${#params[@]}}"
  file_path=$1
  file_name=${file_path##*/}

  if [ "$params_func" ];then 
    exec_step "source $file_path '$params_func'" ${file_name%%.*}
  else
    exec_step "source $file_path" ${file_name%%.*}
  fi
}

exec_source "$kun_main_path/system/debian/安装系统必须项.sh"

exec_source "$kun_main_path/system/初始化/改为国内源.sh 0 y"
exec_step 'apt update' '刷新更新源'
exec_source "$kun_main_path/system/debian/更新系统.sh"

exec_source "$kun_main_path/应用/鲲/依赖项安装.sh"

source "$kun_main_path/lib/function/all"

desc_info=$(described.initialize "{
  名称: '系统环境构建',
  描述: '一条龙式对系统环境进行配置搭建.',
  参数: {
    时区文件路径: {类型: 'string', 默认值:'/usr/share/zoneinfo/Asia/Shanghai', 变量名: ''},
    用户ID: {类型: 'integer', 默认值: 1000, 变量名: 'user_id', 说明: 'Docker中挂载目录时, 文件默认权限为用户ID是1000的用户所有'},
    用户名: {类型: 'string', 默认值:'ubuntu', 变量名: 'user_name', 说明: '\$USER变量值, TODO: 目前dockerfile中进入容器后默认切换至ubuntu'},
    用户组名: {类型: 'string', 默认值:'ubuntu', 变量名: 'group_name'},
    用户组ID: {类型: 'integer', 默认值: 1000, 说明: 'Docker中挂载目录时, 文件默认权限为用户组ID是1000的用户所有', 变量名: 'group_id'},
  }
}")
if [[ ! $? -eq 0 ]]; then
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

result=$(described.validate $desc_info "$@")
if [[ ! $? -eq 0 ]]; then
  echo "$result"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

eval "export $(jq -r '."变量"' <<<"$result")"
eval "echo $(jq -r '."打印参数"' <<<"$result")"
echo $(jq -r '."JSON参数"' <<<"$result")

exec_install 'curl'
exec_install 'vim'
exec_install 'git'
exec_install 'openssh-server'
exec_install 'tmux'
exec_install 'htop'
exec_install 'net-tools'
# exec_install 'apt-utils'
exec_install 'cron' '定时任务'
exec_install 'unzip'

exec_source "$kun_main_path/system/初始化/本地时间.sh"

exec_source "$kun_main_path/system/初始化/新建用户.sh" $(jq -r '."JSON参数"' <<<"$result")

exec_source "$kun_main_path/system/bashrc/标准化bashrc.sh" $(jq -r '."JSON参数"' <<<"$result")

exec_source "$kun_main_path/app/tmux/初始化.sh" $(jq -r '."JSON参数"' <<<"$result")

exec_source "$kun_main_path/system/debian/清理缓存.sh"

dragon_log_config=$(log.initialize "{标题: '序号@命令@描述@结果@说明', 唯一标识: 'dragonService'}")
printf "\\e[2J\\e[H\\e[m"
log.print $dragon_log_config
log.close $dragon_log_config

# sudo bash system/debian/dragonService.sh "$params"
