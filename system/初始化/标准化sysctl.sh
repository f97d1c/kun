#!/bin/bash --login

echo '修改文件描述符数量'
sudo tee -a /etc/sysctl.conf <<-'EOF'
# 修改文件描述符数量
vm.max_map_count=262144
EOF
sudo sysctl -p
if [[ ! $? -eq 0 ]];then echo 'sysctl更新失败';exit 255;fi
echo "完成sysctl更新"