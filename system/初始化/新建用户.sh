#!/bin/bash --login

if [ ! $kun_main_path ]; then
  current_path=$(
    cd $(dirname $0)
    pwd
  )
  kun_main_path="$current_path/../.."
fi

source "$kun_main_path/lib/function/all"

desc_info=$(described.initialize "{
  名称: '新建用户',
  描述: '根据提供信息创建用户相关账号及用户组',
  参数: {
    用户ID: {类型: 'integer', 默认值: 1000, 变量名: 'user_id', 说明: 'Docker中挂载目录时, 文件默认权限为用户ID是1000的用户所有'},
    用户名: {类型: 'string', 默认值:'ubuntu', 变量名: 'user_name'},
    用户组名: {类型: 'string', 默认值:'ubuntu', 变量名: 'group_name'},
    用户组ID: {类型: 'integer', 默认值: 1000, 说明: 'Docker中挂载目录时, 文件默认权限为用户组ID是1000的用户所有', 变量名: 'group_id'},
  }
}")
if [[ ! $? -eq 0 ]]; then
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

res=$(described.validate $desc_info "$@")
if [[ ! $? -eq 0 ]]; then
  echo "$res"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

eval "export $(jq -r '."变量"' <<<"$res")"
eval "echo $(jq -r '."打印参数"' <<<"$res")"

error=''
if [ "$(grep :$user_id: /etc/passwd)" ]; then
  error='用户ID已存在'
elif [ "$(grep $user_name: /etc/passwd)" ]; then
  error='用户名已存在'
elif [ "$(grep $group_name: /etc/group)" ]; then
  error='用户组名已存在'
elif [ "$(grep :$group_id: /etc/group)" ]; then
  error='用户组ID已存在'
fi

if [ "$error" ]; then
  echo $error
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

echo '创建用户及用户组'
groupadd -g $group_id $group_name
if [[ ! $? -eq 0 ]]; then
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

useradd --create-home -s /bin/bash -u $user_id -g $group_id $user_name
if [[ ! $? -eq 0 ]]; then
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

echo '为soduers文件添加写权限'
chmod u+w /etc/sudoers
echo '将新生成的用户组添加到sudoers中并且设置为在执行的时候不输入密码'
echo "%$group_name   ALL=(ALL)   NOPASSWD: ALL" >>/etc/sudoers

echo '新建用户相关内容完成'

# bash system/初始化/新建用户.sh
# bash system/初始化/新建用户.sh "{用户组ID: 'test the user id'}"
# bash system/初始化/新建用户.sh "{用户名: 'web', 用户组名: 'web'}"
# bash system/初始化/新建用户.sh {用户名: 'web', 用户组名: 'web'}
# described.show_how
