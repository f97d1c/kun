#!/bin/bash --login

# 如果未改变检查是否缺失 tzdata
localtime='/usr/share/zoneinfo/Asia/Shanghai'
echo "建立期望的时区链接($localtime)"
sudo ln -fs $localtime /etc/localtime
echo "Asia/Shanghai" | sudo tee /etc/timezone
sudo apt install -fy tzdata
echo '时区设置完成'
echo $(date +%Y年%m月%d日%H时%M分%S秒)