#!/bin/bash --login

if [ ! $kun_main_path ]
then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../../"
fi

can_used=('ubuntu')
system_name=$(cat /etc/issue | awk '{print tolower($1)}')
hardware_platform=$(uname -i)

case "${can_used[@]}" in  
  *"$system_name"*)
    child_script="$kun_main_path/system/初始化/${system_name}_source"
  ;;
  *)
    echo "尚未适配$system_name系统."
    exit 255
  ;;
esac

if [[ ! $? -eq 0 ]];then exit 255;fi

if [ "$1" ];then
  source $child_script "$1"
else
  source $child_script
fi
if [[ ! $? -eq 0 ]];then exit 255;fi


if [ ! "$2" ];then
  echo -e "\n\n"
  read -p "是否写入源文件?(y/n)" choice
else
  choice="$2"
fi

case $choice in
  y|Y|yes|YES)
    source "$kun_main_path/lib/other/backup_file" $source_file_path
    if [[ ! $? -eq 0 ]];then exit 255;fi
    
    echo -e "开始写入源文件"
    echo "$source_conent" | sudo tee $source_file_path
    # 这里如果写入失败应该回滚文件但可能性不大
    if [[ ! $? -eq 0 ]];then echo "$source_file_path 文件写入异常";return 255;fi
    echo "文件更新完成"
  ;;
esac