#!/bin/bash --login

if [ "$kun_path" == '/usr/bin' ]
then
  echo "该脚本仅适用于源码打包"
  exit 255
fi

if [ ! $kun_main_path ]
then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../../"
fi
cd $kun_main_path

# version=$(date +%Y.%m.%d)
version=$(date +%Y.%m.%d.%H%M)
echo "当前版本号: $version"

package_dir="kun-$version"
packaged_name="kun_$version"

echo "创建版本文件夹: $package_dir"
rm -rf $package_dir
mkdir $package_dir

echo "复制脚本至版本文件夹"
cp kun $package_dir
mkdir $package_dir/lib && cp -r lib/* $package_dir/lib
# TODO: 目前添加项目顶级目录后需手动该命令
mkdir $package_dir/app && cp -r app/* $package_dir/app
mkdir $package_dir/system && cp -r system/* $package_dir/system
mkdir $package_dir/容器 && cp -r 容器/* $package_dir/容器
mkdir $package_dir/网络 && cp -r 网络/* $package_dir/网络
mkdir $package_dir/应用 && cp -r 应用/* $package_dir/应用

echo '写入版本号'
touch "$kun_main_path/$package_dir/应用/鲲/版本号.sh"
tee "$kun_main_path/$package_dir/应用/鲲/版本号.sh" <<EOF
#!/bin/bash --login
kun_vsersion="$version"
echo '当前版本: $version'
EOF

cd $package_dir
dh_make --indep --createorig -y
grep -v makefile debian/rules > debian/rules.new
mv debian/rules.new debian/rules
echo kun usr/bin > debian/install
echo lib/ usr/lib/kun >> debian/install
# TODO: 目前添加项目顶级目录后需手动该命令
echo app/ usr/lib/kun >> debian/install
echo system/ usr/lib/kun >> debian/install
echo 容器/ usr/lib/kun >> debian/install
echo 网络/ usr/lib/kun >> debian/install
echo 应用/ usr/lib/kun >> debian/install

echo "1.0" > debian/source/format
rm debian/*.ex
debuild -us -uc

cd ..

package_path="packages/${packaged_name}.deb"
mv "${packaged_name}-1_all.deb" $package_path

echo "删除构建过程内容"
rm -rf $package_dir
reg_name="${packaged_name}*"
# echo "构建结果迁移"
# mkdir $package_dir
# mv $reg_name $package_dir

ls | grep -P "$reg_name" | xargs -d"\n" rm

read -p "已完成打包,是否安装本次打包版本?(y/n)" choice
case ${choice} in
  y|Y|yes|YES)
    echo '卸载旧版本'
    sudo dpkg --remove kun
    echo "安装: $package_path"
    sudo dpkg -i $package_path
  ;;
  *)
    printf "\\e[2J\\e[H\\e[m"
  ;;
esac

echo '更新安装脚本'
install_latest_path="$kun_main_path/packages/install_latest.sh"
rm -f $install_latest_path
tee $install_latest_path <<EOF
#!/bin/bash --login
wget https://gitlab.com/f97d1c/kun/-/raw/master/packages/kun_$version.deb
sudo dpkg -i kun_$version.deb
kun init
EOF