#!/bin/bash --login

echo '[鲲] 依赖项开始安装'

apt-get install -fy bsdmainutils # 将标准输入或文件格式化为多列(column命令)
apt-get install -fy jq
apt-get install -fy nodejs
apt-get install -fy npm
if ! [ -x "$(command -v json)" ]; then
  npm install -g json
fi

# 系统字符集配置
apt-get install -fy locales
locale-gen zh_CN.UTF-8

# TODO: apt-get install -fy nodejs安装的是不是最新的
# echo '安装最新版NodeJs'
# sudo npm cache clean -f
# sudo npm install -g n
# sudo n stable

echo '[鲲] 依赖项完成安装'

# bash 应用/鲲/依赖项安装.sh