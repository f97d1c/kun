#!/bin/bash --login

if [ ! $kun_main_path ];then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

private_ip=$(hostname -I | awk '{print $1}')

echo "export https_proxy=http://$private_ip:7890 http_proxy=http://$private_ip:7890 all_proxy=socks5://$private_ip:7891"

# bash app/clash/终端代理变量.sh