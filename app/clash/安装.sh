#!/bin/bash --login

if [ ! $kun_main_path ]
then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

source "$kun_main_path/lib/function/all"

params="$@"
params=${params:-'{}'}

json '.安装版本' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  clash_version=$(json '.安装版本' <<<"$params")
fi
clash_version=${clash_version:-'0.19.0'}

json '.安装包链接' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  clash_package_url=$(json '.安装包链接' <<<"$params")
fi
clash_package_url=${clash_package_url:-"https://github.com/Dreamacro/clash/releases/download/v$clash_version/clash-linux-amd64-v$clash_version.gz"}

desc_info=$(described.initialize "{
  名称: '安装Clash',
  描述: 'Clash用于科学上网.',
  参数: {
    安装版本: {类型: 'version_number(3)', 默认值: '$clash_version', 变量名: 'clash_version', 说明: '在提供安装包链接时将忽略该参数'},
    安装包链接: {类型: 'url', 默认值: '$clash_package_url', 变量名: 'clash_package_url', 说明: ''},
    安装路径: {类型: 'string', 默认值: '/home/$(id -u -n)/clash', 变量名: 'clash_install_path', 说明: ''},
    全球IP库链接: {类型: 'url', 默认值: 'https://github.com/Dreamacro/maxmind-geoip/releases/download/20201212/Country.mmdb', 变量名: 'clash_country_mmdb_url', 说明: 'Country.mmdb为全球IP库,可以实现各个国家的IP信息解析和地理定位,该数据库每月更新.'},
    UI管理软件包链接: {类型: 'url', 默认值: 'https://github.com/haishanh/yacd/archive/gh-pages.zip', 变量名: 'clash_ui_control_package_url', 说明: '目前仅可用yacd'},
    配置文件路径: {类型: 'url', 默认值: 'https://gitlab.com/f2ce2b/resources/-/raw/master/%E6%95%B0%E6%8D%AE%E6%96%87%E4%BB%B6/%E7%B4%A2%E5%BC%95.json', 变量名: 'clash_config_url', 说明: '用于存放服务器信息的配置文件链接'},
    配置文件层级: {类型: 'string', 默认值: 'LiLnn6Xor4bkvZPns7siLiLlupTnlKjnp5HlraYiLiLorqHnrpfmnLrnp5HlraYiLiLorqHnrpfmnLrns7vnu58iLiLlubbooYzlkozliIbluIPlvI/ns7vnu58iLiJMaW51eCIuImFwcHMiLiJjbGFzaCIuImNvbmZpZ191cmwiLiJsaW51eCIK', 变量名: 'clash_config_level', 说明: 'base64编码格式的字符串,用于存放服务器信息的配置文件链接,原值格式可解码查看(类似: ."知识体系"."应用科学")'},
  }
}")
if [[ ! $? -eq 0 ]]; then
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

res=$(described.validate $desc_info "$@")
if [[ ! $? -eq 0 ]]; then
  echo "$res"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

eval "export $(jq -r '.["变量"]' <<<"$res")"
eval "echo $(jq -r '.["打印参数"]' <<<"$res")"

rescue_config=$(rescue.initialize)

mkdir $clash_install_path 2>/dev/null
cd $clash_install_path

exist_dir="$(ls $clash_install_path/clash* | tr '\n' ' ')"
exist_dir="$(ls $clash_install_path/clash*[^.gz])"
rescue.step $rescue_config '下载Clash安装包' <<EOF
if [ ! "$exist_dir" ];then
  wget -t 5 -c $clash_package_url
else
  echo "已存在$exist_dir,跳过下载步骤"
fi
EOF

rescue.step $rescue_config '下载Country.mmdb' <<EOF
mkdir "$clash_install_path/config"

if [ ! "$(ls $clash_install_path/config)" ];then
  wget -t 5 -c -P "$clash_install_path/config" $clash_country_mmdb_url
else
  echo '已存在$(ls $clash_install_path/config),跳过下载步骤'
fi
EOF

rescue.step $rescue_config '下载UI管理工具' <<EOF
mkdir "$clash_install_path/dashboard"
wget -t 5 -c -P "$clash_install_path/dashboard" $clash_ui_control_package_url
EOF

rescue.step $rescue_config 'Clash安装包解压' <<EOF
gunzip *.gz
chmod +x clash*
EOF

rescue.step $rescue_config 'UI管理工具安装包解压' <<EOF
cd "$clash_install_path/dashboard"
unzip *.zip
EOF

config_url=$(jq "$(base64 -d <<<$clash_config_level)" <<<$(curl -s $clash_config_url))
rescue.step $rescue_config '更新配置文件' <<EOF
curl $config_url >| "$clash_install_path/config/$(date +%Y%m).yaml"
EOF

rescue.result $rescue_config

unset rescue_config
# # bash app/clash/安装.sh