# 此镜像为基础系统镜像
# 仅在官方系统镜像的基础上安装必要/常用基础软件

# 指定基础镜像信息
# ARG BUILD_CNF_PATH DEPOSITORY_NAME SYSTEM_VERSION IMAGE_TAG KUN_DEB_PATH USER_ID USER_NAME GROUP_NAME GROUP_ID
ARG SYSTEM_VERSION

FROM ubuntu:$SYSTEM_VERSION

# FROM前声明的ARG在FROM后无法读取
ARG BUILD_CNF_PATH DEPOSITORY_NAME SYSTEM_VERSION IMAGE_TAG KUN_DEB_PATH USER_ID USER_NAME GROUP_NAME GROUP_ID

# 编写维护者信息
LABEL \
author="mike" \
email="ff4c00@gmail.com"

# 设定默认shell为bash并为交互模式运行
SHELL ["/bin/bash","-ic"]

## 设置环境变量
### 设置系统基本信息 C.UTF-8支持中文
ENV LANG=C.UTF-8

## 复制脚本文件目录
COPY $KUN_DEB_PATH /tmp/
COPY $BUILD_CNF_PATH /tmp/

## 系统初始化相关操作
# 多行构建与一行命令构建结果大小无异且更好定位错误命令位置
RUN dpkg -i "/tmp/${KUN_DEB_PATH##*/}"
RUN rm "/tmp/${KUN_DEB_PATH##*/}"
RUN bash /usr/lib/kun/system/debian/dragonService.sh "$(cat /tmp/${BUILD_CNF_PATH##*/})"
RUN rm "/tmp/${BUILD_CNF_PATH##*/}"

## 设定进入容器后所使用用户
USER $USER_NAME:1000

## 设定进入容器后的默认目录
WORKDIR "/home/$USER_NAME"

# CMD ["source /home/${OPERATOR_USER-$(whoami)}/.bashrc"]
