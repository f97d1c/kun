#!/bin/bash --login

if [ "$kun_path" == '/usr/bin' ]; then
  echo "该脚本仅适用于源码执行"
  exit 255
fi

if [ ! $kun_main_path ]
then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

source "$kun_main_path/lib/function/all"

params="$@"
params=${params:-'{}'}

# jq -e 将导致jq的退出状态反映它是否返回 false 或 null 的内容
jq -e '."系统版本"' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  system_version=$(jq -r '."系统版本"' <<<"$params")
fi
system_version=${system_version:-'18.04'}

# Dockerfile中COPY命令 资源的绝对路径是指构建上下文中的绝对路径，而不是主机上的绝对路径 所以只能在kun目录下执行该脚本
cd "$kun_main_path"

desc_info=$(described.initialize "{
  名称: '构建镜像',
  描述: '根据提供信息创建Ubuntu相关版本Docker镜像',
  参数: {
    构建模式: {类型: 'string', 默认值: '开发', 变量名: 'build_mode', 说明: '可选值[开发/准生产/生产],开发模式(build)下将输出完整的构建日志,准生产模式(buildx)下仅构建当前架构镜像用于验证,生产模式(buildx)下将直接根据[适配平台架构]参数构建成的镜像推送至远程仓库.'},
    仓库名称: {类型: 'string', 默认值: 'ff4c00/linux', 变量名: 'depository_name', 说明: ''},
    适配平台架构: {类型: 'string', 默认值:'linux/amd64,linux/arm64,linux/arm/v7', 变量名: ''},
    时区文件路径: {类型: 'string', 默认值:'/usr/share/zoneinfo/Asia/Shanghai', 变量名: ''},
    系统版本: {类型: 'decimal(2)', 默认值: '$system_version', 说明: '', 变量名: 'system_version'},
    镜像标签: {类型: 'string', 默认值: 'ubuntu-${system_version}-$(date +%Y%m%d)', 说明: '', 变量名: 'image_tag'},
    '鲲': {类型: 'string', 默认值: '$(ls packages/*.deb -Art | tail -n 1)', 说明: '.deb软件包,改动内容构建前注意打包以生效,默认选取最新构建文件.', 变量名: 'kun_deb_path'},
    用户ID: {类型: 'integer', 默认值: 1000, 变量名: 'user_id', 说明: 'Docker中挂载目录时, 文件默认权限为用户ID是1000的用户所有'},
    用户名: {类型: 'string', 默认值:'ubuntu', 变量名: 'user_name', 说明: '\$USER变量值, TODO: 目前dockerfile中进入容器后默认切换至ubuntu'},
    用户组名: {类型: 'string', 默认值:'ubuntu', 变量名: 'group_name'},
    用户组ID: {类型: 'integer', 默认值: 1000, 说明: 'Docker中挂载目录时, 文件默认权限为用户组ID是1000的用户所有', 变量名: 'group_id'},
  }
}")
if [[ ! $? -eq 0 ]]; then
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

res=$(described.test $desc_info "$@")
if [[ ! $? -eq 0 ]]; then
  echo "$res"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

eval "export $(jq -r '."变量"' <<<"$res")"
eval "echo $(jq -r '."打印参数"' <<<"$res")"

echo -n "是否继续构建(y/n)?"
read -e choice
case ${choice} in
n | N | no | No | NO)
  echo "已取消构建,如需对参数值进行调整可通过JSON传参进行调整."
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then
    return 255
  else
    exit 255
  fi
  ;;
esac

docker_file_path="$kun_main_path/app/dockerImage/ubuntu.dockerfile"

if [ ! -f "$kun_deb_path" ]; then
  echo "鲲不存在"
  exit 255
fi

if [[ ! $kun_deb_path =~ .*\.deb$ ]]; then
  echo "鲲路径格式不正确"
  exit 255
fi

# sudo docker buildx build --tag $depository_name:$image_tag --file $docker_file_path --build-arg KUN_DEB_PATH=$kun_deb_path --allow security.insecure --load .

BUILD_CNF_PATH="./build_cnf_$(date +%Y-%m-%d-%H%M-%s).json"
echo $(jq -r '."JSON参数"' <<<"$res") >$BUILD_CNF_PATH

if [ $build_mode == '开发' ]; then
  build_str="docker build \
  -t $depository_name:$image_tag \
  -f $docker_file_path \
  --build-arg $(echo $(jq -r '."ARG变量"' <<<"$res") | awk '{gsub(/\s/, " --build-arg ");print}') \
  --build-arg BUILD_CNF_PATH=$BUILD_CNF_PATH \
  "
else
  build_str="sudo docker buildx build \
  -t $depository_name:$image_tag \
  -f $docker_file_path \
  --build-arg $(echo $(jq -r '."ARG变量"' <<<"$res") | awk '{gsub(/\s/, " --build-arg ");print}') \
  --build-arg BUILD_CNF_PATH=$BUILD_CNF_PATH \
  "
fi

show_how=$(described.show_how $desc_info)

if [ $build_mode == '开发' ]; then
  build_str+="--squash "
elif [ $build_mode == '准生产' ]; then
  build_str+="--load "
elif [ $build_mode == '生产' ]; then
  echo "登录DockerHub账号"
  docker login
  if [[ ! $? -eq 0 ]]; then
    echo 'DockerHub登录失败, 请尝试手动执行: docker login'
    exit 255
  fi
  build_str+="--platform $(jq -r '."适配平台架构"' <<<"$res") --push "
  
  # 未安装多平台模拟器时应先运行 app/docker/初始化Buildx.sh
  echo '启动多平台模拟器'
  docker run --privileged --rm tonistiigi/binfmt --install all
else
  echo -e "$show_how"
  described.error "构建模式错误, 当前值: $build_mode"
  exit 255
fi
build_str+="."

echo "$build_str"
eval "$build_str"

rm -f $BUILD_CNF_PATH

echo -n "是否运行新镜像(y/n)?"
read -e choice
case ${choice} in
y | Y | yes | yes | YES)
  docker run -t -i -v $PWD:/home/$user_name/kun $depository_name:$image_tag
  exit 0
  ;;
*)
  echo "已完成构建"
  exit 0
  ;;
esac

# bash app/dockerImage/ubuntu.sh
# bash app/dockerImage/ubuntu.sh "{构建模式: '准生产'}"
# bash app/dockerImage/ubuntu.sh "{构建模式: '生产'}"
# docker run -t -i -v /home/$USER/SpaceX/知识体系/应用科学/计算机科学/计算机系统/并行和分布式系统/Linux/Shell/kun:/home/ubuntu/kun $(docker images | grep ff4c00/linux | awk 'match($2,/ubuntu-*/){printf "%s:%s\n", $1,$2}' | sort -r | head -n 1)
# docker run -t -i $(docker images | grep ff4c00/linux | awk 'match($2,/ubuntu-*/){printf "%s:%s\n", $1,$2}' | sort -r | head -n 1)