#!/bin/bash --login

if [ "$kun_path" == '/usr/bin' ];then
  echo "该脚本仅适用于源码执行"
  exit 255
fi

if [ ! $kun_main_path ];then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

source "$kun_main_path/lib/function/all"

params="$@"
params=${params:-'{}'}

json '.基础镜像' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  base_image=$(json '.基础镜像' <<<"$params")
fi
base_image=${base_image:-"$(docker images | grep ff4c00/linux | awk 'match($2,/ubuntu-*/){printf "%s:%s\n", $1,$2}' | sort -r | head -n 1)"}

json -e '.Python版本' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  python_version=$(json '.Python版本' <<<"$params")
fi
python_version=${python_version:-'3.9.8'}

jq -e '."虚拟环境名称"' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  virtualenv_name=$(jq -r '."虚拟环境名称"' <<<"$params")
fi
virtualenv_name=${virtualenv_name:-"python-$python_version"}

# ff4c00/linux:ubuntu-18.04-20210907 -> ubuntu_18.04
tag_prefix=$(echo $base_image | sed -r 's/.*\:(\w{1,})\-([0-9]{1,}\.[0-9]{1,}).*/\1_\2/g')

cd "$kun_main_path"

desc_info=$(described.initialize "{
  名称: '构建镜像',
  描述: '根据提供信息创建Python相关版本Docker镜像',
  参数: {
    构建模式: {类型: 'string', 默认值: '开发', 变量名: 'build_mode', 说明: '可选值[开发/准生产/生产],开发模式(build)下将输出完整的构建日志,准生产模式(buildx)下仅构建当前架构镜像用于验证,生产模式(buildx)下将直接根据[适配平台架构]参数构建成的镜像推送至远程仓库.'},
    基础镜像: {类型: 'string', 默认值: '$base_image', 变量名: 'base_image', 说明: ''},
    仓库名称: {类型: 'string', 默认值: 'ff4c00/python', 变量名: 'depository_name', 说明: ''},
    适配平台架构: {类型: 'string', 默认值:'linux/amd64,linux/arm64', 变量名: ''},
    镜像标签: {类型: 'string', 默认值: '$tag_prefix-python_${python_version}-$(date +%Y%m%d)', 说明: '', 变量名: 'image_tag'},
    Python版本: {类型: 'version_number(3)', 默认值: '$python_version', 说明: '', 变量名: 'python_version'},
    pyenv安装位置: {类型: 'string', 默认值: '/home/ubuntu/.pyenv', 说明: '', 变量名: 'pyenv_install_path'},
    虚拟环境名称: {类型: 'string', 默认值: '$virtualenv_name', 说明: '', 变量名: 'virtualenv_name'},
    Http代理: {类型: 'url', 默认值: 'http://127.0.0.1:7890', 说明: '', 变量名: 'http_proxy'},
    Https代理: {类型: 'url', 默认值: 'http://127.0.0.1:7890', 说明: '', 变量名: 'https_proxy'},
    所有代理: {类型: 'string', 默认值: 'socks5://127.0.0.1:7891', 说明: '', 变量名: 'all_proxy'},
  }
}")

if [[ ! $? -eq 0 ]]; then
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

res=$(described.test $desc_info "$@")
if [[ ! $? -eq 0 ]]; then
  echo "$res"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

eval "export $(jq -r '."变量"' <<<"$res")"
eval "echo $(jq -r '."打印参数"' <<<"$res")"

echo -n "是否继续构建(y/n)?"
read -e choice
case ${choice} in
n | N | no | No | NO)
  echo "已取消构建,如需对参数值进行调整可通过JSON传参进行调整."
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then
    return 255
  else
    exit 255
  fi
  ;;
esac

docker_file_path="$kun_main_path/app/dockerImage/python.dockerfile"

BUILD_CNF_PATH="./build_cnf_$(date +%Y-%m-%d-%H%M-%s).json"
echo $(jq -r '."JSON参数"' <<<"$res") >$BUILD_CNF_PATH
jq -r '.' <<<"$res"

# if [ $build_mode == '开发' ]; then
#   build_str="docker build \
#   -t $depository_name:$image_tag \
#   -f $docker_file_path \
#   --build-arg $(echo $(jq -r '."ARG变量"' <<<"$res") | awk '{gsub(/\s/, " --build-arg ");print}') \
#   --build-arg BUILD_CNF_PATH=$BUILD_CNF_PATH \
#   "
# else
#   build_str="sudo docker buildx build \
#   -t $depository_name:$image_tag \
#   -f $docker_file_path \
#   --build-arg $(echo $(jq -r '."ARG变量"' <<<"$res") | awk '{gsub(/\s/, " --build-arg ");print}') \
#   --build-arg BUILD_CNF_PATH=$BUILD_CNF_PATH \
#   "
# fi

# show_how=$(described.show_how $desc_info)

# if [ $build_mode == '开发' ]; then
#   build_str+="--squash "
# elif [ $build_mode == '准生产' ]; then
#   build_str+="--load "
# elif [ $build_mode == '生产' ]; then
#   echo "登录DockerHub账号"
#   docker login
#   if [[ ! $? -eq 0 ]]; then
#     echo 'DockerHub登录失败, 请尝试手动执行: docker login'
#     exit 255
#   fi
#   build_str+="--platform $(jq -r '."适配平台架构"' <<<"$res") --push "

#   # 未安装多平台模拟器时应先运行 app/docker/初始化Buildx.sh
#   echo '启动多平台模拟器'
#   docker run --privileged --rm tonistiigi/binfmt --install all
# else
#   echo -e "$show_how"
#   described.error "构建模式错误, 当前值: $build_mode"
#   exit 255
# fi
# build_str+="."

# echo "$build_str"
# eval "$build_str"

# rm -f $BUILD_CNF_PATH

# echo -n "是否运行新镜像(y/n)?"
# read -e choice
# case ${choice} in
# y | Y | yes | yes | YES)
#   docker run -t -i -v $PWD:/home/$user_name/kun $depository_name:$image_tag
#   exit 0
#   ;;
# *)
#   echo "已完成构建"
#   exit 0
#   ;;
# esac

# # bash app/dockerImage/python.sh
# # bash app/dockerImage/python.sh "{构建模式: '准生产',Python版本:'3.9.8',Http代理:'http://192.168.8.26:7890',Https代理:'http://192.168.8.26:7890',所有代理:'socks5://192.168.8.26:7891'}"
# # bash app/dockerImage/python.sh "{构建模式: '生产',Python版本:'3.9.8',Http代理:'http://192.168.8.26:7890',Https代理:'http://192.168.8.26:7890',所有代理:'socks5://192.168.8.26:7891'}"
# # docker run -t -i $(docker images | grep ff4c00/python | awk 'match($2,/-python*/){printf "%s:%s\n", $1,$2}' | sort -r | head -n 1)