ARG BASE_IMAGE
# 指定基础镜像信息
FROM $BASE_IMAGE

ARG BUILD_CNF_PATH

# 编写维护者信息
LABEL \
author="mike" \
email="ff4c00@gmail.com"

## 复制脚本文件目录
COPY $BUILD_CNF_PATH /tmp/

## 系统初始化相关操作
RUN bash /usr/lib/kun/system/debian/更新系统.sh
# RUN bash /usr/lib/kun/app/python/安装[ubuntu].sh "$(cat /tmp/${BUILD_CNF_PATH##*/})"
RUN kun app python 安装[Ubuntu] "$(cat /tmp/${BUILD_CNF_PATH##*/})"
RUN rm "/tmp/${BUILD_CNF_PATH##*/}"
RUN bash /usr/lib/kun/system/debian/清理缓存.sh