#!/bin/bash --login

if [ ! $kun_main_path ]
then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

source "$kun_main_path/lib/function/all"

params="$@"
params=${params:-'{}'}

desc_info=$(described.initialize "{
  名称: '构建镜像',
  描述: '根据提供信息创建Ruby相关版本Docker镜像',
  参数: {
    Ruby版本: {类型: 'version_number(3)', 默认值: '2.5.3', 说明: '', 变量名: 'ruby_version'},
    Http代理: {类型: 'url', 默认值: 'http://127.0.0.1:7890', 说明: '', 变量名: 'http_proxy'},
    Https代理: {类型: 'url', 默认值: 'http://127.0.0.1:7890', 说明: '', 变量名: 'https_proxy'},
    所有代理: {类型: 'string', 默认值: 'socks5://127.0.0.1:7891', 说明: '', 变量名: 'all_proxy'},
  }
}")
if [[ ! $? -eq 0 ]]; then
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

res=$(described.validate $desc_info "$@")
if [[ ! $? -eq 0 ]]; then
  echo "$res"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

eval "export $(jq -r '."变量"' <<<"$res")"
eval "echo $(jq -r '."打印参数"' <<<"$res")"

# 代理的变量不能存在引号及分号
eval "export https_proxy=$(jq -r '."Http代理"' <<<"$res") http_proxy=$(jq -r '."Https代理"' <<<"$res") all_proxy=$(jq -r '."所有代理"' <<<"$res")"

if [ -x "$(command -v rvm)" ]; then
  if [ "$(rvm list | grep $ruby_version)" ]; then
    echo "Ruby:$ruby_version 已安装, 无需再次安装."
    rvm list
    exit 255
  fi
fi

echo '安装ruby基本环境依赖'
sudo apt update
sudo apt install -fy gnupg2 curl g++ gcc autoconf automake bison libc6-dev libffi-dev libgdbm-dev libncurses5-dev libsqlite3-dev libtool libyaml-dev make pkg-config sqlite3 zlib1g-dev libgmp-dev libreadline-dev libssl-dev nodejs

echo '安装Mysql数据库相关软件'
# 这个必须要安装,不然镜像会大300多MB
sudo apt install -fy mysql-client libmysqlclient-dev

if ! [ -x "$(command -v rvm)" ]; then
  echo '安装RVM'

  curl -sSL https://rvm.io/mpapis.asc | gpg --import -
  curl -sSL https://rvm.io/pkuczynski.asc | gpg --import -
  curl -sSL https://get.rvm.io | bash -s stable
  source ~/.rvm/scripts/rvm

  echo '设置RVM最大超时时间'
  echo "export rvm_max_time_flag=20" >>~/.rvmrc
  source ~/.rvmrc

  echo '安装RVM相关依赖'
  rvm requirements

  echo '修改更新源'
  echo "ruby_url=https://cache.ruby-china.org/pub/ruby" >> ~/.rvm/user/db

  # '解决: You need to change your terminal emulator preferences to allow login shell.'
  echo '[[ -s "/home/$(id -u -n)/.rvm/scripts/rvm" ]] && . "/home/$(id -u -n)/.rvm/scripts/rvm"' >>/home/$(id -u -n)/.bashrc
  source /home/$(id -u -n)/.bashrc
fi

echo "安装Ruby($ruby_version)"
rvm install $ruby_version

# 提示没有rvm命令 暂时注释
# ubuntu server set run command as a login shell
# echo "指定Ruby默认版本($ruby_version)"
# rvm use $ruby_version --default

echo '设置Gem更新源'
gem sources --add https://gems.ruby-china.com/ --remove https://rubygems.org/

echo '安装Bundler'
# 注意版本 已知1.16.2版本会引发: Traceback (most recent call last)
# 可安装指定版本: gem install bundler 1.16.6
gem install bundler

# bash app/ruby/安装.sh