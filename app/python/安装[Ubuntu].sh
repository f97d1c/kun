#!/bin/bash --login

if [ ! $kun_main_path ]
then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

source "$kun_main_path/lib/function/all"

params="$@"
params=${params:-'{}'}

jq -e '."Python版本"' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  python_version=$(jq -r '."Python版本"' <<<"$params")
fi
python_version=${python_version:-'3.9.8'}

jq -e '."虚拟环境名称"' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  virtualenv_name=$(jq -r '."虚拟环境名称"' <<<"$params")
fi
virtualenv_name=${virtualenv_name:-"python-$python_version"}

desc_info=$(described.initialize "{
  名称: '构建镜像',
  描述: '根据提供信息创建Python相关版本Docker镜像',
  参数: {
    Python版本: {类型: 'version_number(3)', 默认值: '$python_version', 说明: '', 变量名: 'python_version'},
    pyenv安装位置: {类型: 'string', 默认值: '/home/$(id -u -n)/.pyenv', 说明: '', 变量名: 'pyenv_install_path'},
    虚拟环境名称: {类型: 'string', 默认值: '$virtualenv_name', 说明: '', 变量名: 'virtualenv_name'},
    Http代理: {类型: 'url', 默认值: 'http://127.0.0.1:7890', 说明: '', 变量名: 'http_proxy'},
    Https代理: {类型: 'url', 默认值: 'http://127.0.0.1:7890', 说明: '', 变量名: 'https_proxy'},
    所有代理: {类型: 'string', 默认值: 'socks5://127.0.0.1:7891', 说明: '', 变量名: 'all_proxy'},
  }
}")
if [[ ! $? -eq 0 ]]; then
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

res=$(described.validate $desc_info "$@")
if [[ ! $? -eq 0 ]]; then
  echo "$res"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

eval "export $(jq -r '."变量"' <<<"$res")"
eval "echo $(jq -r '."打印参数"' <<<"$res")"

# 代理的变量不能存在引号及分号
eval "export https_proxy=$(jq -r '."Http代理"' <<<"$res") http_proxy=$(jq -r '."Https代理"' <<<"$res") all_proxy=$(jq -r '."所有代理"' <<<"$res")"

sudo apt-get update

echo "安装基础依赖项"
sudo apt-get install -fy make build-essential libssl-dev zlib1g-dev \
libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm \
libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev

echo "安装mysql相关依赖项"
sudo apt install -fy mysql-client libmysqlclient-dev

echo "安装pyenv"
if [ ! -d "$pyenv_install_path" ]; then
  git clone git://github.com/yyuu/pyenv.git $pyenv_install_path
else
  echo "[跳过] $pyenv_install_path路径已存在"
fi

echo "安装pyenv-virtualenv"
if [ ! -d "$pyenv_install_path/plugins/pyenv-virtualenv" ]; then
  cd $pyenv_install_path/plugins
  git clone https://github.com/pyenv/pyenv-virtualenv.git
else
  echo "[跳过] $pyenv_install_path/plugins/pyenv-virtualenv路径已存在"
fi

export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
export PATH="/root/.pyenv/shims:${PATH}"
eval "$(pyenv init --path)"

echo '写入pyenv环境变量'
pyenv_env=$(cat /home/$(id -u -n)/.bashrc | grep "pyenv相关内容")
if [ -n "$pyenv_env" ]; then
  echo "[跳过] 已写入相关环境变量"
else

sudo tee -a /home/$(id -u -n)/.bashrc <<-'EOF'

# === pyenv相关内容开始 ===
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
export PATH="/root/.pyenv/shims:${PATH}"
EOF

  echo -e "$(pyenv init --path)\n# === pyenv相关内容结束 ===" >> /home/$(id -u -n)/.bashrc
fi

echo "安装Python: $python_version"
if [ ! -d "$pyenv_install_path/versions/$python_version" ]; then
  pyenv install $python_version
  if [[ ! $? -eq 0 ]];then echo '[失败] Python安装失败';exit 255;fi
else
  echo "[跳过] 已安装$python_version版本"
fi

echo "创建Python虚拟环境: $virtualenv_name"
if [ -n "$(pyenv virtualenvs | grep $virtualenv_name)" ];then
  echo "[跳过] 已存在同名虚拟环境"
else
  pyenv virtualenv $python_version $virtualenv_name
  if [[ ! $? -eq 0 ]];then echo '虚拟环境构建失败';exit 255;fi
fi

if [ ! "$(pyenv virtualenvs | grep $virtualenv_name)" ];then
  echo '[失败] 虚拟环境构建不符预期'
  exit 255
else 
  echo '当前虚拟环境列表:'
  pyenv virtualenvs
fi

echo "切换Python虚拟环境"
pyenv activate $virtualenv_name
if [[ ! $? -eq 0 ]];then echo "[失败] 虚拟环境切换失败";exit 255;fi

current_python_version=$(python -c 'import sys; version=sys.version_info[:3]; print("{0}.{1}.{2}".format(*version))')
if [ ! "$python_version" == "$current_python_version" ]; then
  echo "当前默认Python版本: $current_python_version"
  echo '[失败] Python版本不符预期'
  exit 255
fi

echo '写入pyenv默认启动虚拟环境'
pyenv_env=$(cat /home/$(id -u -n)/.bashrc | grep "pyenv默认启动虚拟环境")
if [ -n "$pyenv_env" ]; then
  echo "[跳过] 已写入pyenv默认启动虚拟环境"
else
echo -e "
# === pyenv默认启动虚拟环境开始 ===
pyenv activate $virtualenv_name
# === pyenv默认启动虚拟环境结束 ===" >> /home/$(id -u -n)/.bashrc
fi

echo "pip更新"
python -m pip install --upgrade pip

pip_python_version=$(pip -V | sed -E 's/.*versions\/python\-([0-9]\.[0-9]\.[0-9])\/lib.*/\1/g')
if [ ! "$python_version" == "$current_python_version" ]; then
  echo '[失败] pip使用版本与当前Python版本不符'
  exit 255
fi

echo "[完成] ..."
# bash app/python/安装[Ubuntu].sh