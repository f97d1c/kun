#!/bin/bash --login

if [ ! $kun_main_path ];then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

source "$kun_main_path/lib/function/all"

params="$@"
params=${params:-'{}'}

desc_info=$(described.initialize "{
  名称: 'SQL转模型定义',
  描述: '根据现有数据库表结构生成对应的django模型定义',
  参数: {
    SQL文件路径: {类型: 'string', 默认值: '', 说明: '现有SQL文件路径', 变量名: 'sql_file_path'},
    迁移管理: {类型: 'string', 默认值: '否', 说明: '停用自动迁移管理功能后仅生成迁移文件并不进行执行操作', 变量名: 'migration_managed'},
    单行最大长度: {类型: 'integer', 默认值: 500, 说明: '当文件中单行内容长度大于该值时将跳过处理', 变量名: 'line_max_length'},
  }
}")
if [[ ! $? -eq 0 ]]; then
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

res=$(described.validate $desc_info "$@")
if [[ ! $? -eq 0 ]]; then
  echo "$res"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

eval "export $(jq -r '."变量"' <<<"$res")"
eval "echo $(jq -r '."打印参数"' <<<"$res")"

echo -e "\n"
# cat $sql_file_path

reg_table_begin="^CREATE TABLE .*"
reg_column_varchar=".* varchar\([0-9]+\) .*"
reg_column_text=".* text .*"
reg_column_longtext=".* longtext .*"

reg_column_tinyint=".* tinyint\([0-9]+\) .*"
reg_column_smallint=".* smallint\([0-9]+\) .*"
reg_column_int=".* int\([0-9]+\) .*"
reg_column_bigint=".* bigint\([0-9]+\) .*"

reg_column_float=".* float .*"
reg_column_decimal=".* decimal\([0-9]+, [0-9]+\) .*"
reg_column_date=".* date .*"
reg_column_datetime=".* datetime\([0-9]+\) .*"
reg_column_bit=".* bit\([0-9]+\) .*"
reg_column_index=".*INDEX \`.*"
reg_table_unique_index=".* UNIQUE INDEX .*"
reg_table_primary_key=".* PRIMARY KEY .*"
reg_table_end=".*\) ENGINE .*"
reg_column_not_null=".*NOT NULL.*"
reg_column_default=".* DEFAULT .*"
reg_table_comment=".*COMMENT = '.*'.*"
reg_column_comment=".*COMMENT '.*'.*"

in_table=0

function handle_column_not_null() {
  if [[ "$line" =~ $reg_column_not_null ]]; then
    present_flag=' '
  else
    present_flag=' null=True, blank=True, '
  fi

  if [[ "$line" =~ $reg_column_default ]]; then
    default_value=$(sed -r 's/.* DEFAULT ([^ |,]*).*/\1/g' <<<$line)
    if [ "$default_value" == 'NULL' ]; then
      default_value='None'
    fi
    present_flag="${present_flag}default=$default_value, "
  fi

  echo "$present_flag"
}

function handle_table_begin() {
  in_table=1
  tmpfile=$(mktemp -t "django_XXXXXXX.py") || exit 1
  table_name=$(sed -r 's/CREATE TABLE `(.*)`.*/\1/g' <<<$line)
  class_name=$(echo $table_name | sed "s/_/ /g" | sed 's/\b[a-z]/\U&/g' | sed "s/ //g")
  echo "class $class_name(models.Model):" >>$tmpfile
}

function handle_column_varchar() {
  max_length=$(sed -r 's/.*varchar\(([0-9]+)\).*/\1/g' <<<$line)

  if [ "$column_name" == 'id' ]; then
    echo "    id ¥ models.CharField(primary_key=True, editable=False, max_length=$max_length,${present_flag}verbose_name='$column_comment')" >>$tmpfile
  else
    echo "    $column_name ¥ models.CharField(max_length=$max_length,${present_flag}verbose_name='$column_comment')" >>$tmpfile
  fi

}

function handle_column_text() {
  echo "    $column_name ¥ models.TextField(${present_flag}verbose_name='$column_comment')" >>$tmpfile
}

function handle_column_int() {
  echo "    $column_name ¥ models.IntegerField(${present_flag}verbose_name='$column_comment')" >>$tmpfile
}

function handle_column_bigint() {

  if [[ "$line" =~ .*AUTO_INCREMENT.* ]]; then
    echo "    $column_name ¥ models.BigAutoField(primary_key=True, editable=False,${present_flag}verbose_name='$column_comment')" >>$tmpfile
  else
    echo "    $column_name ¥ models.BigIntegerField(${present_flag}verbose_name='$column_comment')" >>$tmpfile
  fi
}

function handle_column_float() {
    echo "    $column_name ¥ models.FloatField(${present_flag}verbose_name='$column_comment')" >>$tmpfile
}

function handle_column_decimal() {
  max_digits=$(sed -r 's/.*decimal\(([0-9]+), ([0-9]+)\).*/\1/g' <<<$line)
  decimal_places=$(sed -r 's/.*decimal\(([0-9]+), ([0-9]+)\).*/\2/g' <<<$line)
  echo "    $column_name ¥ models.DecimalField(${present_flag}max_digits=$max_digits, decimal_places=$decimal_places, verbose_name='$column_comment')" >>$tmpfile
}

function handle_column_date() {
  echo "    $column_name ¥ models.DateField(${present_flag}verbose_name='$column_comment')" >>$tmpfile
}


function handle_column_datetime() {
  echo "    $column_name ¥ models.DateTimeField(${present_flag}verbose_name='$column_comment')" >>$tmpfile
}

function handle_column_bit() {
  max_length=$(sed -r 's/.*bit\(([0-9]+)\).*/\1/g' <<<$line)
  echo "    $column_name ¥ models.BinaryField(max_length=$max_length, ${present_flag}verbose_name='$column_comment')" >>$tmpfile
}

function handle_table_unique_index() {
  # TODO: 这里不一定是两个字段的联合索引
  index_name=$(sed -r "s/.*\`(.*)\`\(\`(.*)\`, \`(.*)\`\).*/\1/g" <<<$line)
  index_column_1=$(sed -r "s/.*\`(.*)\`\(\`(.*)\`, \`(.*)\`\).*/\2/g" <<<$line)
  index_column_2=$(sed -r "s/.*\`(.*)\`\(\`(.*)\`, \`(.*)\`\).*/\3/g" <<<$line)
  echo "    models.UniqueConstraint(fields=['$index_column_1', '$index_column_2'], name='$index_name')" >>$tmpfile
}

function handle_column_index() {
  # line='INDEX `IDX_NAME`(`name`, `is_delete`) USING BTREE'
  # line='INDEX `data_type`(`data_type`) USING BTREE'

  reg_column_index_='[ ]*INDEX ([a-z A-Z _ `]*)([a-z A-Z _ ` \( \) \,]*) USING BTREE[,]*'
  index_name_=$(sed -r "s/$reg_column_index_/\1/g" <<<$line)
  index_columns_=$(sed -r "s/$reg_column_index_/\2/g" <<<$line)
  index_name=$(sed -r 's/`(.*)`/\1/g' <<<$index_name_)
  
  reg_index_columns='.*`, `.*'
  if [[ $index_columns_ =~ $reg_index_columns ]];then
    index_columns_=$(sed -r 's/\(`(.*)`\)/\1/g' <<<"$index_columns_")
    index_columns_=$(sed 's/`, `/ /g' <<<"$index_columns_")
    index_columns=$(sed "s/ /', '/g" <<<"$index_columns_")

    # echo "    models.Index(fields=['$index_columns'], name='$index_name')" >>$tmpfile
    #echo "index_columns: $index_columns, index_name: $index_name"
    #echo "    models.Index(fields=[''], name='')" >>$tmpfile
  else
    index_column=$(sed -r 's/\(`(.*)`\)/\1/g' <<<"$index_columns_")
    #echo "index_columns: $index_column, index_name: $index_name"
    # echo "    models.Index('$index_column', name='$index_name')" >>$tmpfile
    #echo "    models.Index('', name='')" >>$tmpfile
  fi
}

function handle_table_end() {
  in_table=0

  echo -e "\n" >>$tmpfile
  echo "    class Meta:" >>$tmpfile
  if [ "$migration_managed" == "否" ]; then 
    echo "        managed ¥ False" >>$tmpfile
  fi
  echo "        db_table ¥ '$table_name'" >>$tmpfile

  if [[ "$line" =~ $reg_table_comment ]]; then
    table_comment=$(sed -r "s/.*COMMENT = '(.*)'.*/\1/g" <<<$line)
    echo "        verbose_name ¥ '$table_comment'" >>$tmpfile
    echo "        verbose_name_plural ¥ '$table_comment'" >>$tmpfile
  fi


  echo -e "\n" >>$tmpfile

  error_lines=$(cat $tmpfile | grep \`)
  if [[ $? -eq 0 ]]; then
    printf "\033[0;31m\33[7m[运行时异常] 存在处理异常内容(\`):\n$error_lines\33[0m\033[0m\n"
    rm -f "$tmpfile"
    if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
  fi

  error_lines=$(cat $tmpfile | grep ,,)
  if [[ $? -eq 0 ]]; then
    printf "\033[0;31m\33[7m[运行时异常] 存在处理异常内容(,,):\n$error_lines\33[0m\033[0m\n"
    rm -f "$tmpfile"
    if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
  fi

  error_lines=$(cat $tmpfile | grep 运行时异常)
  if [[ $? -eq 0 ]]; then
    printf "\033[0;31m\33[7m[运行时异常] 存在处理异常内容(运行时异常):\n$error_lines\33[0m\033[0m\n"
    rm -f "$tmpfile"
    if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
  fi

  cat $tmpfile | sed "s/( /(/g" | sed "s/¥/¥=/g"| column -s '¥' -t | sed "s/class Meta:/\\n    class Meta:/g"

  rm -f "$tmpfile"
}

while IFS= read -r line || [ -n "$line" ]; do

  if [[ ${#line} -gt $line_max_length ]];then
    if [[ $in_table -eq 1 ]];then
      echo "单行内容长度(${#line})大于单行最大长度($line_max_length), 跳过该行内容处理: ${line:0:$line_max_length}"
    fi
    continue
  fi

  column_name=$(sed -r 's/  `(.*)`.*/\1/g' <<<$line)
  if [[ "$line" =~ $reg_column_comment ]];then
    column_comment=$(sed -r "s/.*COMMENT '(.*)'.*/\1/g" <<<$line)
  else
    column_comment=${column_name^^}
  fi
  
  present_flag=$(handle_column_not_null "$line")

  if [[ "$line" =~ $reg_table_begin ]]; then
    handle_table_begin
  elif [[ "$line" =~ $reg_column_varchar ]]; then
    handle_column_varchar
  elif [[ "$line" =~ $reg_column_text ]]; then
    handle_column_text
  elif [[ "$line" =~ $reg_column_longtext ]]; then
    handle_column_text
  elif [[ "$line" =~ $reg_column_tinyint ]]; then
    handle_column_int
  elif [[ "$line" =~ $reg_column_smallint ]]; then
    handle_column_int
  elif [[ "$line" =~ $reg_column_int ]]; then
    handle_column_int
  elif [[ "$line" =~ $reg_column_bigint ]]; then
    handle_column_bigint
  elif [[ "$line" =~ $reg_column_float ]]; then
    handle_column_float
  elif [[ "$line" =~ $reg_column_decimal ]]; then
    handle_column_decimal
  elif [[ "$line" =~ $reg_column_date ]]; then
    handle_column_date
  elif [[ "$line" =~ $reg_column_datetime ]]; then
    handle_column_datetime
  elif [[ "$line" =~ $reg_column_bit ]]; then
    handle_column_bit
  elif [[ "$line" =~ $reg_column_index ]]; then
    handle_column_index
  elif [[ "$line" =~ $reg_table_unique_index ]]; then
    handle_table_unique_index
  elif [[ "$line" =~ $reg_table_primary_key ]]; then
    echo ''
  elif [[ "$line" =~ $reg_table_end ]]; then
    handle_table_end
  else
    if [[ $in_table -eq 1 ]] && [ -n "$line" ]; then
      echo "[运行时异常] 未匹配行: $line" >>$tmpfile
      # printf "\033[0;31m\33[7m[运行时异常] 未匹配行: $line\33[0m\033[0m\n"
    fi
  fi
done <$sql_file_path

# bash app/python/[Django]SQL转模型定义.sh "{'SQL文件路径': '/home/ff4c00/space/code/yggc/python_web/test.sql'}"