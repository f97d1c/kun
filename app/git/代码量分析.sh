#!/bin/bash --login

if [ ! $kun_main_path ]; then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

source "$kun_main_path/lib/function/all"

params="$@"
params=${params:-'{}'}

jq -e '.["唯一标识"]' <<<$(similar_json.format "$params") 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  uniq_id=$(jq -r '.["唯一标识"]' <<<$(similar_json.format "$params"))
fi

desc_info=$(described.initialize "{
  名称: '代码量分析',
  描述: '根据GitLog对指定时间范围内的代码变动进行分析.',
  参数: {
    配置文件路径: {类型: 'string', 默认值: '$(cd `dirname $0`;pwd)/项目信息.json', 变量名: 'project_config_path', 说明: '配置文件路径'},
    开始时间: {类型: 'date', 默认值: '$(date +%Y-%m-01)', 变量名: 'begin_at', 说明: '分析时间范围'},
    结束时间: {类型: 'date', 默认值: '$(date +%Y-%m-31)', 变量名: 'end_at', 说明: '分析时间范围'},
  }
}")
if [[ ! $? -eq 0 ]]; then
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

res=$(described.validate $desc_info "$@")
if [[ ! $? -eq 0 ]]; then
  echo "$res"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

eval "export $(jq -r '."变量"' <<<"$res")"
eval "echo $(jq -r '."打印参数"' <<<"$res")"

project_config=$(cat $project_config_path)
keys=($(jq -r 'keys_unsorted[]' <<<$project_config))

log_config=$(log.initialize "{标题: '项目#代码总计#分支#新增(行)#删除(行)#新增率(%)#代码生产率', 分隔符: '#'}")

for key in "${keys[@]}"; do
  info=$(jq -r ".\"${key}\"" <<<"$project_config")
  branchs=($(jq -r '."分支"|.[]' <<<$info))

  project_name=$(jq -r '."名称"' <<<$info)
  # echo "分析$project_name(${key})"
  cd ${key}
  
  for branch in "${branchs[@]}"; do
    # echo "解析$branch分支"
    git checkout -q $branch

    if [[ ! $? -eq 0 ]];then 
      log.set $log_config "$project_name#异常#$branch####"
      continue
    fi

    all_datas=$(git log --since="1995-03-22" --before="$end_at" --pretty=tformat: --numstat | awk '{ add += $1; subs += $2; loc += $1 + $2 } END { printf "%s::%s::%s", add, subs, loc }')
    all_datas=(${all_datas//::/ })
    # 增加行数
    all_add=${all_datas[0]}
    # 删除行数
    all_remove=${all_datas[1]}
    # 总行数
    all_total=${all_datas[2]}

    range_datas=$(git log --since="$begin_at" --before="$end_at" --pretty=tformat: --numstat | awk '{ add += $1; subs += $2; loc += $1 + $2 } END { printf "%s::%s::%s", add, subs, loc }')
    range_datas=(${range_datas//::/ })
    
    if [ ! ${#part_datas[*]} -eq 3 ]; then
      range_total=0 
      range_rate=0
      range_rate_100='0%'
      range_remove=0
      # 项目#代码总计#分支#新增(行)#删除(行)#新增率(%)#代码生产率
      log.set $log_config "$project_name#$all_total#$branch#$range_total#$range_remove#$range_rate#$range_rate_100"
      continue
    fi

    # 增加行数
    range_add=${range_datas[0]}
    # 删除行数
    range_remove=${range_datas[1]}
    # 总行数
    range_total=${range_datas[2]}
    # 新增率
    range_rate="0$(echo "scale=8;$range_total/$all_total" | bc)"
    range_rate_100="0$(echo "scale=8;$range_total/$all_total*100" | bc)%"

    log.set $log_config "$project_name#$all_total#$branch#$range_total#$range_remove#$range_rate#$range_rate_100"

  done
done

echo -e "\n\n$begin_at - $end_at 时间段内,项目代码变动情况概览"

log.print $log_config 

# bash app/git/代码量分析.sh