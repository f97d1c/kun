#!/bin/bash --login

if [ ! $kun_main_path ]; then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

source "$kun_main_path/lib/function/all"

params="$@"
params=${params:-'{}'}
params=$(similar_json.format "$params")

# jq -e 将导致jq的退出状态反映它是否返回 false 或 null 的内容
jq -e '."仓库名称"' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  depository_name=$(jq -r '."仓库名称"' <<<"$params")
fi
depository_name=${depository_name:-'ff4c00/linux'}

jq -e '."容器名称"' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  container_name=$(jq -r '."容器名称"' <<<"$params")
fi

jq -e '."挂载路径"' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  mount_path=$(jq -r '."挂载路径"' <<<"$params")
  container_name=${container_name:-${mount_path##*/}}
fi

container_name=${container_name:-$(echo $RANDOM | md5sum | head -c 20)}

desc_info=$(described.initialize "{
  名称: '创建容器',
  描述: '根据提供信息创建相关镜像实例',
  参数: {
    仓库名称: {类型: 'string', 默认值: '$depository_name', 变量名: 'depository_name', 说明: ''},
    实例镜像: {类型: 'string', 默认值:'$(docker images | grep $depository_name | awk 'match($2,/ubuntu-*/){printf "%s:%s\n", $1,$2}' | sort -r | head -n 1)', 变量名: 'depository_image_name', 说明: '默认为本地仓库最新镜像信息'},
    控制台交互: {类型: 'string', 默认值:'是', 变量名: 'interactive'},
    终端登录: {类型: 'string', 默认值: '是', 变量名: 'tty'},
    系统路径: {类型: 'string', 默认值: '无', 变量名: 'system_path', 说明: '宿主机需挂载文件路径'},
    挂载路径: {类型: 'string', 默认值: '无', 变量名: 'mount_path', 说明: '容器内挂载路径'},
    容器名称: {类型: 'string', 默认值: '$container_name', 变量名: 'container_name', 说明: ''},
    映射端口: {类型: 'string', 默认值: '无', 变量名: 'mapping_ports', 说明: '宿主端口:容器内端口'},
  }
}")
if [[ ! $? -eq 0 ]]; then
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

res=$(described.test $desc_info "$@")
if [[ ! $? -eq 0 ]]; then
  echo "$res"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

eval "export $(jq -r '."变量"' <<<"$res")"
eval "echo $(jq -r '."打印参数"' <<<"$res")"

exec_str='docker run'

if [ "$interactive" == "是" ]; then exec_str+=' -i';fi
if [ "$tty" == "是" ]; then exec_str+=' -t';fi
exec_str+=" --name $container_name"
if [ "$system_path" != "无" ] && [ "$mount_path" != "无" ]; then exec_str+=" -v $system_path:$mount_path";fi
if [ "$mapping_ports" != "无" ];then exec_str+=" -p $mapping_ports";fi
exec_str+=" $depository_image_name"

echo -e "\n$exec_str"

echo -n "是否执行上述命令(y/n)?"
read -e choice
case ${choice} in
n | N | no | No | NO)
  exit 255
esac

eval "$exec_str"

# bash app/docker/新建容器.sh "{仓库名称:'ff4c00/ruby', 系统路径: '/home/ff4c00/space/code/yggc/shucaikeji', 挂载路径: '/home/ubuntu/shucaikeji'}"
# bash app/docker/新建容器.sh "{仓库名称:'ff4c00/ruby', 系统路径: '/home/ff4c00/space/code/yggc/shucaikeji', 挂载路径: '/home/ubuntu/shucaikeji', 映射端口: '3111:3000'}"
# bash app/docker/新建容器.sh