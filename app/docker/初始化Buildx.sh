#!/bin/bash --login

echo '安装Buildkit'
docker pull moby/buildkit

echo '安装并启动多平台模拟器(全部)'
docker run --privileged --rm tonistiigi/binfmt --install all

echo '创建基于docker-container驱动的builder实例kun_builder'

docker buildx create --name kun_builder --driver docker-container

echo '切换至kun_builder'
docker buildx use kun_builder