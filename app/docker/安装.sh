#!/bin/bash --login

if [ ! $kun_main_path ]
then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

print_content='-'
source "$kun_main_path/lib/info/系统信息.sh"

echo '添加国内软件仓库'
case $hardware_platform in
  'x86_64')
    sudo add-apt-repository "deb [arch=amd64] https://mirrors.aliyun.com/docker-ce/linux/ubuntu $(lsb_release -cs) stable"
  ;;
  'aarch64')
    sudo add-apt-repository "deb [arch=armhf] https://mirrors.tuna.tsinghua.edu.cn/docker-ce/linux/ubuntu $(lsb_release -cs) stable"
  ;;
  *)
    echo "尚未兼容当前硬件架构($hardware_platform)."
    return 255
  ;;
esac

echo '安装docker.io及docker-compose'
sudo apt-get install -fy docker.io docker-compose