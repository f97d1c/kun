#!/bin/bash --login

if [ ! $kun_main_path ]; then
  current_path=$(
    cd $(dirname $0)
    pwd
  )
  kun_main_path="$current_path/../.."
fi

params="$@"
params=${params:-"{}"}
user_name=$(json ".用户名" <<<"$params")
user_name=${user_name:-"$(id -u -n)"}

if [ ! "$user_name" ]; then
  echo '用户名不能为空'
  exit 255
fi

if ! [ -x "$(command -v tmux)" ]; then
  # TODO: 后期这里需要针对不同发行版本进行适配
  echo "安装tmux"
  sudo apt update
  sudo apt install tmux -y
fi
# app/tmux/tmux.conf
echo "复制tmux配置文件"
sudo cp "$kun_main_path/app/tmux/tmux.conf" "/home/$user_name/.tmux.conf"
if [[ ! $? -eq 0 ]]; then
  echo "配置文件复制失败"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

echo '刷新tmux配置文件'
tmux source-file "/home/$user_name/.tmux.conf" 2>/dev/null

echo "tmux完成初始化"

# bash app/tmux/初始化.sh 
# bash app/tmux/初始化.sh '{"用户名": "kun"}'