#!/bin/bash --login

if [ ! $kun_main_path ]; then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

source "$kun_main_path/lib/function/all"

params="$@"
params=${params:-'{}'}
params=$(similar_json.format "$params")

target_path="/home/$USER/Space/知识体系/精神文明/音乐/流派/流行"

asset_path="/home/$USER/Space/资源体系/音频文件/歌曲"

song_info_path='/home/$USER/Space/资源体系/音频文件/资源平台/网易云/歌曲详情.json'

standard_version=20211015

song_infos=$(jq --arg standard_version $standard_version '."\($standard_version)"' $song_info_path)

tags="[\"${PWD##*/}\"]"
find *163* -type f -print0 | while IFS= read -r -d $'\0' file; do 
  old_name=${file##*/}
  song_id=$(echo "$old_name" | sed -E "s/.*id => \"([0-9]*)\".*/\1/g")
  song_name=$(echo "$old_name" | sed -E "s/.*name => \"([^\"]*)\".*/\1/g")

  song_info=$(jq -r --arg song_id $song_id '."\($song_id)"' <<<$song_infos)
  if [ ! "$song_info" ]; then echo "尚未收录 $song_name($song_id)相关歌曲信息";continue;fi
  if [ "$song_info" == "null" ]; then echo "尚未收录 $song_name($song_id)相关歌曲信息";continue;fi
  if [ ! "$(jq -r '."歌曲名称"' <<<$song_info)" == "$song_name" ]; then echo "$song_name($song_id)相关歌曲信息不符";continue;fi

  md5_value=$(cat "$old_name" | md5sum)
  md5_value=${md5_value:0:32}
  
  new_name="{名称=>$(jq '."歌曲名称"' <<<$song_info), 标签=>$tags, 类型=>[\"音频\", \"歌曲\"], 其他=>{歌手=>$(echo $(jq '."歌手名称"' <<<$song_info) | sed -E 's/ \"|\" /"/g'), 标识=>\"$song_id\", 来源=>\"网易云\"}, 版本=>20211015}.${old_name##*.}"
  # new_name=$(echo $new_name | sed "s/'/\"/g")
  
  standard_doc_name.test "$new_name"
  if [[ ! $? -eq 0 ]]; then echo "[跳过] $old_name"; continue; fi
  
  eval "mv $(printf '%q' "$old_name") $(printf '%q' "$new_name")"
done



find */*$standard_version* -type f -print0 | while IFS= read -r -d $'\0' file; do 
  eval "mv $(printf '%q' "$file") "$asset_path/""
done

# bash app/网易云/格式化标准文件名称.sh