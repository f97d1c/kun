#!/bin/bash --login

if [ ! $kun_main_path ]; then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

source "$kun_main_path/lib/function/all"

params="$@"
params=${params:-'{}'}
params=$(similar_json.format "$params")

jq -e '."歌单文件存储地址"' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  store_playlists_path=$(jq -r '."歌单文件存储地址"' <<<"$params")
fi
store_playlists_path=${store_playlists_path:-"/home/$USER/Space/资源体系/音频文件/资源平台/网易云/歌单详情.json"}

jq -e '."歌曲文件存储地址"' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  store_songs_path=$(jq -r '."歌曲文件存储地址"' <<<"$params")
fi
store_songs_path=${store_songs_path:-"${store_playlists_path%/*}/歌曲详情.json"}

jq -e '."标准文件名称版本号"' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  standard_version=$(jq -r '."标准文件名称版本号"' <<<"$params")
fi
standard_version=${standard_version:-$(jq '."资源体系"."现行标准文件名称版本号"' <<<$(space_config.result))}

jq -e '."歌单标识"' <<<"$params" 1>/dev/null 2>/dev/null
if [[ $? -eq 0 ]]; then
  playlist_ids=$(jq -r '."歌单标识"' <<<"$params")
fi
playlist_ids=${playlist_ids:-"$(echo $(jq -r '."资源体系"."音频文件"."资源平台"."网易云"."涉及歌单标识"|.[]' <<<$(space_config.result)))"}

desc_info=$(described.initialize "{
  名称: '创建容器',
  描述: '根据提供信息创建相关镜像实例',
  参数: {
    歌单文件存储地址: {类型: 'string', 默认值: '$store_playlists_path', 变量名: 'store_playlists_path', 说明: ''},
    歌曲文件存储地址: {类型: 'string', 默认值:'$store_songs_path', 变量名: 'store_songs_path', 说明: ''},
    标准文件名称版本号: {类型: 'integer', 默认值:'$standard_version', 变量名: 'standard_version'},
    歌单标识: {类型: 'string', 默认值: '$playlist_ids', 变量名: 'playlist_ids'},
  }
}")
if [[ ! $? -eq 0 ]]; then
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

res=$(described.test $desc_info "$@")
if [[ ! $? -eq 0 ]]; then
  echo "$res"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

eval "export $(jq -r '."变量"' <<<"$res")"
eval "echo $(jq -r '."打印参数"' <<<"$res")"

playlist_ids=($playlist_ids)

# 每写入一定行数对临时文件进行备份 当最新文件混乱后删除即可
# 根据最后修改时间对临时文件排序 并选取最近更改文件使用
tmp_store_playlists_path=$(find /tmp/kun_netease_* -printf "%T@ %Tc %p\n" 2>/dev/null | sort -n  | tail -n 1 | awk '{print $5}')
if [ ! "$tmp_store_playlists_path" ]; then
  tmp_store_playlists_path=$(mktemp -t 'kun_netease_XXXXXXX.json') || exit 1
  echo "{\"$standard_version\":{" >| $tmp_store_playlists_path
  for playlist_id in "${playlist_ids[@]}"; do
    echo "\"$playlist_id\":{}," >> $tmp_store_playlists_path
  done
  sed -i '$s/,//g' $tmp_store_playlists_path
  echo '}}' >> $tmp_store_playlists_path
fi

for playlist_id in "${playlist_ids[@]}"; do
  playlist_info=$(jq --arg standard_version $standard_version --arg playlist_id $playlist_id '."\($standard_version)"."\($playlist_id)"' $tmp_store_playlists_path)

  if [[ ! $(jq 'keys | length' <<<$playlist_info) -eq 0 ]]; then continue;fi

  playlist_result=$(curl -s "https://api.imjad.cn/cloudmusic/?type=playlist&id=$playlist_id")
  if [[ ! $(jq '.code' <<<$playlist_result) -eq 200 ]]; then echo "歌单查询返回码异常, playlist_id=$playlist_id"; exit 255; fi

  playlist=$(jq '.playlist' <<<$playlist_result)
  if [ ! "$playlist" ];then echo '歌单信息为空'; exit 255;fi

  playlist_info="{
    \"歌单标识\":$playlist_id,
    \"歌单名称\":$(jq '.name' <<<$playlist),
    \"歌单封面\":$(jq '.coverImgUrl' <<<$playlist),
    \"歌单标签\":$(jq '.tags' <<<$playlist),
    \"歌曲数量\":$(jq '.trackIds | length' <<<$playlist),
    \"歌曲标识\":$(jq '.trackIds | map(.id)' <<<$playlist),
    \"歌曲详情\": {}
  }"

  printf "初始化歌单: "; echo $playlist_info

  echo $(jq --arg standard_version $standard_version --arg playlist_id $playlist_id --argjson playlist_info "$playlist_info" '."\($standard_version)"."\($playlist_id)" = $playlist_info' $tmp_store_playlists_path) >| $tmp_store_playlists_path
done

function store_song_info (){
  local playlist_id=$1
  local song_id=$2
  local store_path=$3
  local progress=$4
  local loop_count=$5
  local standard_version=$6

  local song_result=$(curl -s "https://api.imjad.cn/cloudmusic/?type=detail&id=$song_id")
  if [[ ! $(jq '.code' <<<$song_result) -eq 200 ]]; then echo '歌曲查询返回码异常'; return 255; fi

  local song=$(jq '.songs[0]' <<<$song_result)

  local song_info="{
  \"歌曲标识\":$(jq '.id' <<<$song),
  \"歌曲名称\":$(jq '.name' <<<$song),
  \"歌手标识\":$(jq '.ar | map(.id)' <<<$song),
  \"歌手名称\":$(jq '.ar | map(.name)' <<<$song),
  \"专辑标识\":$(jq '.al.id' <<<$song),
  \"专辑名称\":$(jq '.al.name' <<<$song),
  \"专辑封面\":$(jq '.al.picUrl' <<<$song)
  }"

  echo -e "$(date '+%Y-%m-%d %H:%M:%S') | $progress | $(echo $song_info)"

  echo $(jq --arg standard_version $standard_version --arg playlist_id $playlist_id --arg song_id "$song_id" --argjson song_info "$song_info" '."\($standard_version)"."\($playlist_id)"."歌曲详情"."\($song_id)" = $song_info' $store_path) >| $store_path

  # if [[ ! $? -eq 0 ]]; then echo $exec_str; jq '.' <<<$song_info; return 255; fi

  # 每100行对文件进行备份
  if [[ $(expr $loop_count % 100) -eq 0 ]]; then
    source "$kun_main_path/lib/other/backup_file" $store_path
  fi
}

loop_count=0
for playlist_id_index in "${!playlist_ids[@]}"; do
  playlist_id=${playlist_ids[$playlist_id_index]}

  playlist_info=$(jq --arg standard_version $standard_version --arg playlist_id $playlist_id '."\($standard_version)"."\($playlist_id)"' $tmp_store_playlists_path)
  song_size=$(jq '."歌曲数量"' <<<$playlist_info)
  playlist_song_info=$(jq '."歌曲详情"' <<<$playlist_info)
  current_song_size=$(jq 'keys | length' <<<$playlist_song_info)

  if [[ $current_song_size -eq $song_size ]]; then continue; fi
  
  song_ids=($(jq -r '."歌曲标识" | .[]' <<<$playlist_info))
  progress_playlist=$(expr $playlist_id_index + 1)

  for song_id_index in "${!song_ids[@]}"; do
    song_id=${song_ids[$song_id_index]}
    jq -e --arg song_id $song_id '."\($song_id)"' <<<$playlist_song_info 1>/dev/null
    if [[ $? -eq 0 ]]; then echo "[跳过] 已存在 $song_id"; continue; fi
    progress="$progress_playlist/${#playlist_ids[@]} - $(expr $song_id_index + 1)/${#song_ids[@]}"
    
    flag=255
    while [[ ! $flag -eq 0 ]]; do
      loop_count=$(expr $loop_count + 1)
      store_song_info $playlist_id $song_id $tmp_store_playlists_path "$progress" $loop_count $standard_version
      flag=$?
      if [[ ! $flag -eq 0 ]];then 
        second=$((10 + $RANDOM % 50))
        echo "歌单标识: $playlist_id, 歌曲标识: $song_id, 获取失败, 休眠$second秒, 等待重试.."
        sleep $second
      fi
    done

  done

  echo -e "$(jq '.' $tmp_store_playlists_path)" >| $store_playlists_path
done

source "$kun_main_path/lib/other/backup_file" $store_playlists_path
if [[ ! $? -eq 0 ]];then exit 255;fi

if [ ! -f "$store_songs_path" ]; then
  printf '{}' >| $store_songs_path
fi

for playlist_id in "${playlist_ids[@]}"; do
  song_infos=$(jq --arg standard_version $standard_version --arg playlist_id $playlist_id '."\($standard_version)"."\($playlist_id)"."歌曲详情"' $store_playlists_path)

  song_ids=($(jq -r 'keys_unsorted[]' <<<$song_infos))
  for song_id in "${song_ids[@]}"; do
    song_info=$(jq --arg song_id $song_id '."\($song_id)"' <<<$song_infos)
    # TODO: jq 对于变量长度有一定要求 目前采取单条写入
    new_content=$(jq --arg standard_version $standard_version --arg song_id $song_id --argjson song_info "$song_info" '."\($standard_version)"."\($song_id)" = $song_info' $store_songs_path)
    if [[ ! $? -eq 0 ]]; then echo "$song_id 歌曲数据生成异常"; continue ;fi
    echo $new_content >| $store_songs_path
  done
done

echo -e "$(jq '.' $store_songs_path)" >| $store_songs_path
source "$kun_main_path/lib/other/backup_file" $store_songs_path
if [[ ! $? -eq 0 ]];then exit 255;fi

# # rm -f $(ls /tmp/kun_netease_*)
# # bash app/网易云/歌单内歌曲标识获取.sh