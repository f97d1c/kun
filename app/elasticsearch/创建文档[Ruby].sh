#!/bin/bash --login
if [ ! $kun_main_path ]
then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../../"
fi

if ! [ -x "$(command -v ruby)" ]; then
  echo "系统尚未安装Ruby"
else
  ruby "$kun_main_path/app/elasticsearch/创建文档.rb"
fi