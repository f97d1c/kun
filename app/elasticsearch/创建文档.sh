#!/bin/bash --login

if [ ! $kun_main_path ]; then
  current_path=$(
    cd $(dirname $0)
    pwd
  )
  kun_main_path="$current_path/../../"
fi
source "$kun_main_path/app/elasticsearch/params"
source "$kun_main_path/app/elasticsearch/requestES.sh" "source_mode"
# bash app/elasticsearch/创建文档.sh -f "/home/$USER/Space/知识体系/人文科学/经济学/金融/微观金融学/金融体系/金融工具/基金/数据研究/基础数据/fund_infos.txt" -i 'fund_infos'
# bash app/elasticsearch/requestES.sh -r "fund_infos/_doc/_search"

if [ ! $index_name ]; then echo "索引名称(-i)参数不能为空."; exit 255;fi
if [ ! $file_path ]; then echo "文件路径(-f)参数不能为空."; exit 255;fi
if [ ! -f $file_path ]; then echo "文件不存在或非普通文件($file_path)."; exit 255;fi

requestES 'GET' "$host:$port/$index_name/$type_name/_search"
if [[ ! $? -eq 0 ]];then echo "索引不存在($index_name/$type_name)"; exit 255;fi

while IFS= read -r line || [ -n "$line" ]; do
  md5=$(echo -n "$line" |md5sum)
  route="$index_name/$type_name/${md5:0:32}"
  reload_params
  body="$line"
  method='POST'
  requestES
  # print_table ',' $print_content
  # requestES POST "daily_market_funds/_doc/${md5:0:32}" "$line"
done < $file_path
