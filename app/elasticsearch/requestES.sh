#!/bin/bash --login

if [ ! $kun_main_path ]; then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../../"
fi

source "$kun_main_path/app/elasticsearch/params"

# bash app/elasticsearch/requestES.sh -h 127.0.0.1 -p 9201
# bash app/elasticsearch/requestES.sh -h 127.0.0.1 -p 9201 -m post
# bash app/elasticsearch/requestES.sh -h 127.0.0.1 -p 9201 -m post -r "wdba_projects_finished/doc/_search"
# bash app/elasticsearch/requestES.sh -h 127.0.0.1 -p 9201 -m get -r "wdba_projects_finished/doc/_search"
# bash app/elasticsearch/requestES.sh -h 127.0.0.1 -p 9201 -m post -r "wdba_projects_finished/doc/_search" -b '{"size": 1}'

function requestES () {
  method_=$1
  requestUrl_=$2
  body_=$3

  method=${method_:-$method}
  requestUrl=${requestUrl_:-$requestUrl}
  body=${body_:-$body}

  case "$method" in
    'GET'|'Get'|'get')
      result=$(curl -Ss -X $method $requestUrl)
      if [[ ! $? -eq 0 ]];then return 255;fi
      ;;
    'POST'|'Post'|'post'|'PUT'|'Put'|'put')
      if [ $body ]; then
        result=$(curl -Ss -H "Content-Type: application/json" -X $method $requestUrl -d "$body")
        if [[ ! $? -eq 0 ]];then return 255;fi
      else
        result=$(curl -Ss -H "Content-Type: application/json" -X $method $requestUrl)
        if [[ ! $? -eq 0 ]];then return 255;fi
      fi
      ;;
    'DELETE'|'Delete'|'delete')
      result=$(curl -Ss -X $method $requestUrl)
      if [[ ! $? -eq 0 ]];then return 255;fi
      ;;
    *)
      echo "未知请求类型($method)"
      return 255
      ;;
  esac
  print_content+="结束时间,$(date '+%Y-%m-%dT%H:%M:%S.%N')\n"

  if [ "$result" ];then
    if ! [ -x "$(command -v json)" ]; then
      echo '格式化处理需要安装json(npm install -g json)'
      # 127(255/2)表示部分成功
      retrun 127
    fi
  else
    print_table ',' $print_content
    retrun 255 
  fi

  error=$(echo "$result" | json '.error')
  if [ -n "$error" ]; then
    success='false'
  else
    success='true'
  fi
  print_content+="请求处理成功,$success\n"

  timed_out=$(echo "$result" | json '.timed_out')
  if [ $timed_out ]; then print_content+="是否超时,$timed_out\n"; fi

  # hits_total=$(echo "$result" | json '.hits.total')
  # if [ $hits_total == '{']; then 
  #   hits_total=$(echo "$result" | json '.hits.total.value')
  # fi
  # if [ $hits_total ]; then print_content+="命中总数,$hits_total\n"; fi

  # 存在数组为空情况 但是里面无法用(||{})._index
  origin_index_name=$(echo $(echo "$result" | json ".hits.hits.['0']._index"))
  if [ $origin_index_name ]; then print_content+="原始索引名称,$origin_index_name\n"; fi

  print_table ',' $print_content

  if [ $success == 'false' ]; then
    if [ $body ]; then echo $body | json ; fi
  fi

  echo "$result"
  echo ""
}

if [ $1 == 'source_mode' ]; then return 0; fi

requestES
if [[ ! $? -eq 0 ]];then exit 255;fi