#!/bin/bash --login

echo -n "输入端口号:";read -e port;

listen=$(lsof -iTCP -sTCP:LISTEN -P | grep :${port};)
if [ ! "$listen" ];then 
  echo '未发现端口被占用'
  exit 255
fi

arr=(${listen// / })
# echo ${arr[@]}

if [ ! "${arr[1]}" ];then 
  echo '进程ID不存在'
  exit 255
fi

kill -9 ${arr[1]}

listen=$(lsof -iTCP -sTCP:LISTEN -P | grep :${port};)
if [ ! "$listen" ];then 
  echo '端口已释放'
  exit 0
else
  echo "端口仍被占用: $listen"
  exit 255
fi
