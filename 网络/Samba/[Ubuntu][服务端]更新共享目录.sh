#!/bin/bash --login

if [ ! $kun_main_path ]; then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

kun_cnf_params=$(source "$kun_main_path/应用/鲲/更新配置文件.sh")

samba_cnf_params=$(jq -r --arg hostname $(hostname) '
  .["知识体系"]["应用科学"]["计算机科学"]["计算机系统"]["计算机网络"]["网络文件系统"]["SAMBA"]["服务端"]["\($hostname)"]
' <<<$kun_cnf_params)

if [ ! "$samba_cnf_params" ]; then
  echo "尚不存在关于主机: $(hostname),相关服务端配置信息."
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

exports_samba_cnf_params=$(jq -r '.["共享目录"]' <<<$samba_cnf_params)

if [ ! "$exports_samba_cnf_params" ]; then
  echo "尚不存在关于主机: $(hostname),相关服务端共享目录信息."
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

conf_path='/etc/samba/smb.conf'

sudo /bin/bash -c "source '$kun_main_path/lib/other/backup_file' $conf_path"

printf '' | sudo tee $conf_path

export_dirs=($(jq -r 'keys_unsorted[]' <<<$exports_samba_cnf_params))

OLD_IFS="$IFS"
IFS=$'\n'

for dir in ${export_dirs[@]}; do
  export_contents=$(jq -r --arg dir $dir '.["\($dir)"]' <<<$exports_samba_cnf_params)
  export_content_keys=($(jq -r '. | keys_unsorted[]' <<<$export_contents))
  echo -e "\n\n[$dir]" | sudo tee -a $conf_path
  for key in ${export_content_keys[@]}; do
    value=$(jq -r --arg key $key '.["\($key)"]' <<<$export_contents)
    echo "  $key = $value" | sudo tee -a $conf_path
  done
done

IFS="$OLD_IFS"

sudo service smbd restart