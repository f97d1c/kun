#!/bin/bash --login

if ! [ -x "$(command -v samba)" ]; then
  sudo apt update
  sudo apt install -y samba
  sudo ufw allow samba
else
  echo "已安装"
fi