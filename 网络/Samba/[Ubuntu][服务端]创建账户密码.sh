#!/bin/bash --login
# Samba不使用系统帐户密码

user_name=$1
user_name=${user_name:-"$(id -u -n)"}

echo "为用户($user_name)创建密码:"
sudo smbpasswd -a $user_name