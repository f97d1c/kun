#!/bin/bash --login

if [ ! $kun_main_path ]; then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

# source "$kun_main_path/lib/function/all"

conf_path='/etc/dnsmasq.conf'

if ! [ -f "$conf_path" ]; then
  echo '开始安装Dnsmasq'
  sudo apt update
  sudo apt install -y dnsmasq
else
  echo '已安装Dnsmasq'
fi

if ! [ -x "$(command -v dig)" ]; then
  echo '开始安装dnsutils'
  sudo apt update
  sudo apt install -y dnsutils
fi