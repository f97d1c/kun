#!/bin/bash --login

if [ ! $kun_main_path ]; then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

conf_path='/etc/dnsmasq.conf'
test_dns='test-local-dns.com'
dns_config_path='/etc/my_dns.conf'
local_private_ip=$(hostname -I | awk '{print $1}')

# 基础配置文件 后面有需要做成项目通用的变量
space_config=$(curl -s 'https://gitlab.com/f2ce2b/resources/-/raw/master/%E6%95%B0%E6%8D%AE%E6%96%87%E4%BB%B6/%E7%B4%A2%E5%BC%95.json')

dns_cnf_space_config=$(jq -r '
  .["知识体系"]["应用科学"]["计算机科学"]["计算机系统"]["计算机网络"]["域名系统"]
' <<<$space_config)

public_dns_address=$(jq -r '.["公共DNS"]["地址"]' <<<$dns_cnf_space_config)
local_domain_cnf=$(jq -r '.["自建服务器"]["自定义域名"]' <<<$dns_cnf_space_config)

# 存在这个文件 下面配置的resolv-file不起作用
sudo rm -f /etc/resolv.conf

sudo /bin/bash -c "source '$kun_main_path/lib/other/backup_file' $conf_path"

echo -e "\n编辑配置文件: $conf_path"

dns_ips=($(jq -r 'keys_unsorted[]' <<<$public_dns_address))

sudo tee "$conf_path" <<EOF
resolv-file=$dns_config_path
strict-order
cache-size=10000
listen-address=127.0.0.1,$local_private_ip

address=/$test_dns/$local_private_ip
EOF


defined_local_address=($(jq -c -r --arg hostname $(hostname) '[.["\($hostname)"]["指向本地"]] | flatten(1) | .[]' <<<$local_domain_cnf))

for address in "${defined_local_address[@]}"; do
    echo "address=/$address/$local_private_ip" | sudo tee -a $conf_path
done

defined_hosts=($(jq -c -r --arg hostname $(hostname) '.["\($hostname)"] | [keys_unsorted[]] | map(select(. != "指向本地")) | .[]' <<<$local_domain_cnf))

for host in "${defined_hosts[@]}"; do
    defined_host_address=($(jq -c -r --arg hostname $(hostname) --arg host $host '[.["\($hostname)"]["\($host)"]] | flatten(1) | .[]' <<<$local_domain_cnf))
    for address in "${defined_host_address[@]}"; do
      echo "address=/$address/$host" | sudo tee -a $conf_path
    done
done


echo '' | sudo tee -a $conf_path

for ip in "${dns_ips[@]}"; do
  array=($(jq -c -r --arg key $ip '[.["\($key)"]] | flatten(1) | .[]' <<<$public_dns_address))
  if [[ ${#array[@]} -eq 0 ]];then continue; fi

  for domain in "${array[@]}"; do
    if [ $domain == '' ];then continue; fi
    if [ $domain == 'null' ];then continue; fi
    echo "server=/$domain/$ip" | sudo tee -a $conf_path
  done  
done

echo -e "\n编辑上游DNS服务器配置文件: $dns_config_path"

sudo tee "$dns_config_path" <<EOF
nameserver 127.0.0.1
nameserver 223.5.5.5
$(for ip in "${dns_ips[@]}"; do
  echo "nameserver $ip"
done)
EOF

echo -e "\n重启Dnsmasq"
sudo service dnsmasq restart

echo -e "\n查看Dnsmasq状态"
echo -e "\n$(sudo service dnsmasq status)"


if [[ "$(dig $test_dns +short)" == "$local_private_ip" ]];then
  echo -e "\n配置正常"
else
  echo -e "\n配置异常, $test_dns 配置未生效"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

index_files=($(find /home/$(id -u -n) -path "*资源体系/数据文件/索引.json" 2>/dev/null))
if [[ ! ${#index_files[@]} -eq 0 ]];then 
  index_file=${index_files[0]}
  echo -e "\n更新局域网DNS服务器IP"

  result=$(jq --arg hostname $(hostname) --arg local_private_ip $local_private_ip '
    .["知识体系"]["应用科学"]["计算机科学"]["计算机系统"]["计算机网络"]["域名系统"]["自建服务器"]["局域网"]
    ["\($hostname)"] = $local_private_ip
  ' $index_file)

  jq -e '.' <<< $result 1>/dev/null 2>/dev/null

  if [[ $? -eq 0 ]]; then 
    echo "$result" > $index_file
  else
    echo -e "\n$index_file 文件格式异常,停止更新."
  fi
  
fi
