#!/bin/bash --login

if [ ! $kun_main_path ]; then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

conf_path="/home/$(id -n -u)/.ssh/config"

sudo /bin/bash -c "source '$kun_main_path/lib/other/backup_file' $conf_path"

sudo tee "$conf_path" <<EOF
Host gitlab.com
  HostName gitlab.com
  User git
  # ProxyCommand bash -c "nc -v -x $(dig vpn.proxy.local +short):7891 %h %p"
  ServerAliveInterval 60
  ServerAliveCountMax 5

Host *
     ServerAliveInterval 86400
     ServerAliveCountMax 4
EOF

