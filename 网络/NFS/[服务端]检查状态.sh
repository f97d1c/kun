#!/bin/bash --login

if ! [ -f "/etc/init.d/nfs-kernel-server" ]; then
  echo '尚未安装nfs-kernel-server'
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

sudo /etc/init.d/nfs-kernel-server status