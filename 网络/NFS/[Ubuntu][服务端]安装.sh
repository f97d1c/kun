#!/bin/bash --login

sudo apt update
sudo apt-get install -y rpcbind

if ! [ -f "/etc/init.d/nfs-kernel-server" ]; then
  echo '开始安装nfs-kernel-server'
  sudo apt install -y nfs-kernel-server
else
  echo '已安装nfs-kernel-server'
fi

# TODO: 关于防火墙开启状态下NFS端口相关配置尚未处理