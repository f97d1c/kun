#!/bin/bash --login

if ! [ -f "/etc/init.d/nfs-kernel-server" ]; then
  echo '尚未安装nfs-kernel-server'
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

if [ ! $kun_main_path ]; then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

kun_cnf_params=$(source "$kun_main_path/应用/鲲/更新配置文件.sh")

nfs_cnf_params=$(jq -r --arg hostname $(hostname) '
  .["知识体系"]["应用科学"]["计算机科学"]["计算机系统"]["计算机网络"]["网络文件系统"]["NFS"]["服务端"]["\($hostname)"]
' <<<$kun_cnf_params)

if [ ! "$nfs_cnf_params" ]; then
  echo "尚不存在关于主机: $(hostname),相关服务端配置信息."
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

exports_nfs_cnf_params=$(jq -r '.["共享目录"]' <<<$nfs_cnf_params)

if [ ! "$exports_nfs_cnf_params" ]; then
  echo "尚不存在关于主机: $(hostname),相关服务端共享目录信息."
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

conf_path='/etc/exports'

sudo /bin/bash -c "source '$kun_main_path/lib/other/backup_file' $conf_path"

printf '' | sudo tee $conf_path

export_dirs=($(jq -r 'keys_unsorted[]' <<<$exports_nfs_cnf_params))

for dir in ${export_dirs[@]}; do
  export_hosts=($(jq -r --arg dir $dir '.["\($dir)"] | keys_unsorted[]' <<<$exports_nfs_cnf_params))

  for host in ${export_hosts[@]}; do
    is_domain=$(echo "$host" | grep -P "(?=^.{4,253}$)(^(?:[a-zA-Z0-9](?:(?:[a-zA-Z0-9\-]){0,61}[a-zA-Z0-9])?\.)+([a-zA-Z]{2,}|xn--[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])$)")

    # dns自定义域名nfs无法解析 这里将其转为IP
    if [ "$is_domain" ];then
      host_ip=$(dig "$is_domain" +short)
      if [ ! "$host_ip" ];then
        echo "跳过, 无法解析 $host 对应IP地址"
        continue;
      fi
    fi
    
    export_options_=($(jq -r -c --arg dir $dir --arg host $host '.["\($dir)"]["\($host)"] | .[]' <<<$exports_nfs_cnf_params))

    export_options=$(printf "%s," "${export_options_[@]}")
    if [ "$is_domain" ];then
      echo "$dir $host_ip($export_options)" | sudo tee -a $conf_path
    else
      echo "$dir $host($export_options)" | sudo tee -a $conf_path
    fi

  done

done

sudo exportfs -rv

# bash 网络/NFS/[服务端]更新共享目录.sh