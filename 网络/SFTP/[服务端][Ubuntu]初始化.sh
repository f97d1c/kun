#!/bin/bash --login

cnf_path='/etc/ssh/sshd_config'

if [ ! "$(cat $cnf_path | grep 'Match group sftp')" ];then
sudo tee -a "$cnf_path" <<'EOF'
Match group sftp
  ChrootDirectory /home
  X11Forwarding no
  AllowTcpForwarding yes
  ForceCommand internal-sftp
EOF

echo '重启SSH服务'
sudo systemctl restart ssh
fi

group_name='sftp'
user_name='sftp_user'

if [ ! "$(grep $group_name: /etc/group)" ]; then
  echo '添加sftp用户组'
  sudo addgroup sftp
fi

if [ ! "$(grep $user_name: /etc/passwd)" ]; then
  echo 'sftp用户组新增用户'
  sudo useradd -m $user_name -g $group_name

  read -s -p "新用户密码:" password
  echo "$user_name:$password" | sudo chpasswd

  echo '新用户文件夹权限调整'
  sudo chmod 700 /home/$user_name/
  # sudo chmod 777 -R /home/sftp_user/
fi
